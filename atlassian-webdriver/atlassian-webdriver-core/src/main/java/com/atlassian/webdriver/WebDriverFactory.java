package com.atlassian.webdriver;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.fugue.retry.ExceptionHandler;
import com.atlassian.fugue.retry.RetryFactory;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.webdriver.browsers.chrome.ChromeBrowser;
import com.atlassian.webdriver.browsers.edge.EdgeBrowser;
import com.atlassian.webdriver.browsers.firefox.FirefoxBrowser;
import com.atlassian.webdriver.browsers.ie.IeBrowser;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.function.Supplier;

import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_BROWSER;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_BROWSER_HEADLESS;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_INIT_RETRY;

/**
 * Checks the System property webdriver.browser to see what browser driver to return. Defaults to Firefox.
 */
public class WebDriverFactory {
    private static final Logger log = LoggerFactory.getLogger(WebDriverFactory.class);


    private static final int GET_DRIVER_RETRY_COUNT = WEBDRIVER_INIT_RETRY.getSystemPropertyAsInt(1);

    private WebDriverFactory() {
    }

    public static WebDriverContext getDriverContext() {
        return getDriverContext(null);
    }

    public static String getBrowserProperty() {
        return WEBDRIVER_BROWSER.getSystemProperty("firefox");
    }

    public static boolean isBrowserHeadless() {
        return WEBDRIVER_BROWSER_HEADLESS.getSystemPropertyAsBool();
    }

    public static Browser getBrowser() {
        final String browserProperty = getBrowserProperty();
        return getBrowser(browserProperty);
    }

    public static Browser getBrowser(String browserProperty) {
        if (RemoteWebDriverFactory.matches(browserProperty)) {
            return RemoteWebDriverFactory.getBrowser(browserProperty);
        }
        return LocalWebDriverFactory.getBrowser(browserProperty);
    }

    public static WebDriverContext getDriverContext(final BrowserConfig browserConfig) {
        final Supplier<WebDriverContext> runnable = () -> getDriverNoRetry(browserConfig);
        final ExceptionHandler exceptionLogger = ex -> {
            log.warn("Error creating WebDriver object, retrying.. ({})", ex.toString());
            log.debug("Error creating WebDriver object - stack trace", ex);
        };
        return RetryFactory.create(runnable::get, GET_DRIVER_RETRY_COUNT, exceptionLogger).get();
    }

    private static WebDriverContext getDriverNoRetry(BrowserConfig browserConfig) {
        final String browserProperty = getBrowserProperty();

        if (RemoteWebDriverFactory.matches(browserProperty)) {
            log.info("Loading RemoteWebDriverFactory driver {}", browserProperty);
            return RemoteWebDriverFactory.getDriverContext(browserProperty);
        }

        final String browserPath = LocalWebDriverFactory.getBrowserPath(browserProperty);

        // find a driver for a given browser type
        Browser browserType = LocalWebDriverFactory.getBrowser(browserProperty);

        // parse webdriver.capabilities and webdriver.logging.* properties
        final MutableCapabilities capabilities = DesiredCapabilitiesFactory.defaultCapabilitiesForBrowser(browserType);
        final DesiredCapabilities customCapabilities = DesiredCapabilitiesFactory.customCapabilitiesFromSystemProperty();
        final DesiredCapabilities loggingCapabilities = DesiredCapabilitiesFactory.customCapabilitiesForLogging();
        capabilities.merge(customCapabilities);
        capabilities.merge(loggingCapabilities);

        WebDriver driver = getBrowserDriver(browserType, browserConfig, browserPath, capabilities);
        if (driver == null) {
            log.error("Unknown browser: {}, defaulting to firefox.", browserProperty);
            browserType = Browser.FIREFOX;
            driver = new FirefoxBrowser().getDriver();
        }

        return new DefaultWebDriverContext(driver, browserType);
    }

    @Nullable
    private static WebDriver getBrowserDriver(final Browser browserType,
                                              final BrowserConfig browserConfig,
                                              final String browserPath,
                                              @Nullable final Capabilities capabilities) {
        switch (browserType) {
            case FIREFOX:
                return FirefoxBrowser.getDriver(browserConfig, browserPath, capabilities);

            case CHROME:
                return ChromeBrowser.getDriver(browserConfig, browserPath, capabilities);

            case EDGE:
                return EdgeBrowser.createEdgeDriver(browserPath, browserConfig);

            default:
                return null;
        }
    }
}
