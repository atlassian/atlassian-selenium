package com.atlassian.webdriver.debug;

import com.atlassian.webdriver.browsers.firefox.FirefoxBrowser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Since FF does not support webdriver logs api eg. webDriver.get().manage().logs().getAvailableLogTypes()
 * I'm following advice from <a href="https://github.com/mozilla/geckodriver/issues/284#issuecomment-458305621">...</a>
 * I'm storing console output to file and reading the console errors.
 *
 * @since 3.1.4
 */
class FirefoxLogParsingJavaScriptErrorRetriever implements JavaScriptErrorRetriever {
    private static final Logger log = LoggerFactory.getLogger(FirefoxLogParsingJavaScriptErrorRetriever.class);

    // Default regex used to filter browser console logs for errors
    private static final String DEFAULT_ERROR_REGEX = "(?:console\\.error|JavaScript error): (?<error>.*)";
    // Default regex match group used to extract error message from browser console logs
    private static final String REGEX_GROUP_NAME = "error";

    private Pattern pattern = Pattern.compile(DEFAULT_ERROR_REGEX, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    private final String filePath;
    int alreadyProcessedLines = 0;

    public FirefoxLogParsingJavaScriptErrorRetriever(@Nullable String filePath) {
        this.filePath = filePath;
    }

    public FirefoxLogParsingJavaScriptErrorRetriever(@Nullable String filePath, @Nullable Pattern pattern) {
        this.filePath = filePath;
        if (pattern != null) {
            this.pattern = pattern;
        }
    }

    @Override
    public boolean isErrorRetrievalSupported() {
        return this.filePath != null;
    }

    @Override
    public Iterable<JavaScriptErrorInfo> getErrors() {
        if (this.filePath == null) {
            log.warn("getErrors() called with empty 'filePath', have 'isErrorRetrievalSupported' been called prior this invocation ?");
            return Collections.emptyList();
        }
        log.debug("Getting console logs from file: {} and skipping {}", this.filePath, alreadyProcessedLines);
        try {
            final Path path = Paths.get(this.filePath);
            return Files.lines(path)
                    .skip(alreadyProcessedLines)
                    .map(line -> {
                        alreadyProcessedLines++;
                        return line;
                    })
                    .map(line -> this.pattern.matcher(line))
                    .filter(Matcher::find)
                    .map(matcher -> matcher.group(REGEX_GROUP_NAME))
                    .map(line -> new JavaScriptErrorInfoImpl(line, line))
                    .collect(Collectors.toList());
        } catch (IOException | SecurityException e) {
            log.error("Error during processing firefox log file {}", this.filePath, e);
            return Collections.emptyList();
        }
    }

    /**
     * @return log file path defined by {@link FirefoxBrowser#FIREFOX_WEBDRIVER_LOGFILE} system property.
     */
    @Nullable
    public static String getFilePathFromSystemProperty() {
        return System.getProperty(FirefoxBrowser.FIREFOX_WEBDRIVER_LOGFILE);
    }

    /**
     * NOTE: {@link FirefoxBrowser#FIREFOX_WEBDRIVER_DEVTOOLS_CONSOLE_REGEX} must include a group named "error"
     * @return log regex pattern defined by {@link FirefoxBrowser#FIREFOX_WEBDRIVER_DEVTOOLS_CONSOLE_REGEX} system property.
     */
    public static Pattern getLogRegexPatternFromSystemProperty() {
        String regex = System.getProperty(FirefoxBrowser.FIREFOX_WEBDRIVER_DEVTOOLS_CONSOLE_REGEX, DEFAULT_ERROR_REGEX);
        if (!regex.isEmpty()) {
            return Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        }
        return Pattern.compile(DEFAULT_ERROR_REGEX, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
    }
}
