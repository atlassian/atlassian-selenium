package com.atlassian.webdriver.debug;

import java.util.Objects;

public class JavaScriptErrorInfoImpl implements JavaScriptErrorInfo {
    private final String description;
    private final String message;

    public JavaScriptErrorInfoImpl(String description, String message) {
        this.description = description;
        this.message = message;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "JavaScriptErrorInfoImpl{" +
                "description='" + description + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JavaScriptErrorInfoImpl that = (JavaScriptErrorInfoImpl) o;
        return Objects.equals(description, that.description) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, message);
    }
}
