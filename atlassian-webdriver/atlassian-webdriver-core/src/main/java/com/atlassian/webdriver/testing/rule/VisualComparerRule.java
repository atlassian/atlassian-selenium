package com.atlassian.webdriver.testing.rule;

import com.atlassian.selenium.visualcomparison.VisualComparableClient;
import com.atlassian.selenium.visualcomparison.VisualComparer;
import com.atlassian.webdriver.visualcomparison.WebDriverVisualComparableClient;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import jakarta.inject.Inject;

import static com.atlassian.webdriver.testing.rule.WebDriverSupport.fromAutoInstall;

/**
 * @since 2.1
 */
public class VisualComparerRule extends FailsafeExternalResource
{
    private final WebDriver driver;
    private final JavascriptExecutor javascriptExecutor;

    private VisualComparer comparer;

    @Inject
    public VisualComparerRule(WebDriver driver, JavascriptExecutor javascriptExecutor)
    {
        this.driver = driver;
        this.javascriptExecutor = javascriptExecutor;
    }

    public VisualComparerRule()
    {
        this(fromAutoInstall().getDriver(), (JavascriptExecutor) fromAutoInstall().getDriver());
    }

    @Override
    protected void before() throws Throwable
    {
        VisualComparableClient client = new WebDriverVisualComparableClient(driver, javascriptExecutor);
        comparer = new VisualComparer(client);
    }

    public VisualComparer getComparer()
    {
        return comparer;
    }
}
