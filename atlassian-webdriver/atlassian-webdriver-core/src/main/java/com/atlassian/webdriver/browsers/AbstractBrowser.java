package com.atlassian.webdriver.browsers;

import com.atlassian.browsers.BrowserConfig;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.annotation.Nullable;

public abstract class AbstractBrowser<D extends RemoteWebDriver> {

    public static <D extends RemoteWebDriver, B extends AbstractBrowser<D>> D getDriver(
            final Class<B> clazz,
            final BrowserConfig browserConfig,
            final String browserPath,
            @Nullable final Capabilities capabilities) {
        final D driver;
        final AbstractBrowser<D> browserFactory = newInstance(clazz);
        if (browserPath == null && browserConfig != null) {
            driver = browserFactory.getDriver(browserConfig, capabilities);
        } else if (browserPath != null) {
            driver = browserFactory.getDriver(browserPath, capabilities);
        } else {
            driver = browserFactory.getDriver(capabilities);
        }
        return driver;
    }

    private static <D extends RemoteWebDriver, B extends AbstractBrowser<D>> B newInstance(
            final Class<B> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    public abstract D getDriver(@Nullable final Capabilities capabilities);

    public abstract D getDriver(final BrowserConfig browserConfig,
                                @Nullable final Capabilities capabilities);

    public abstract D getDriver(final String browserPath,
                                @Nullable final Capabilities capabilities);

    public D getDriver() {
        return getDriver((Capabilities) null);
    }

    public D getDriver(final BrowserConfig browserConfig) {
        return getDriver(browserConfig, null);
    }

    public D getDriver(final String browserPath) {
        return getDriver(browserPath, null);
    }

}
