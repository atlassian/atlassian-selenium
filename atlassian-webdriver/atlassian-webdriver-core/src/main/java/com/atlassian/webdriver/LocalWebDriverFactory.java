package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocalWebDriverFactory {
    /** Matches a browser type with a path */
    private static final Pattern browserPathPattern = Pattern.compile("^([A-Za-z0-9_.-]+):path=(.*)$");

    public static Browser getBrowser(String browserProperty) {
        final Matcher matcher = browserPathPattern.matcher(browserProperty);
        if (matcher.matches()) {
            return Browser.typeOf(matcher.group(1));
        }
        return Browser.typeOf(browserProperty);
    }

    public static String getBrowserPath(String browserProperty) {
        final Matcher matcher = browserPathPattern.matcher(browserProperty);
        if (matcher.matches()) {
            return matcher.group(2);
        }
        return null;
    }
}
