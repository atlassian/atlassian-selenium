package com.atlassian.webdriver.browsers.firefox;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.webdriver.WebDriverProperties;
import com.atlassian.webdriver.browsers.AbstractBrowser;
import com.atlassian.webdriver.browsers.profile.ProfilePreferences;
import com.atlassian.webdriver.utils.WebDriverUtil;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.webdriver.WebDriverFactory.isBrowserHeadless;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_FIREFOX_SWITCHES;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_HTTP_FACTORY;
import static org.apache.commons.lang3.SystemUtils.IS_OS_WINDOWS;

/**
 * A helper utility for obtaining a FirefoxDriver.
 *
 * @since 2.0
 */
public final class FirefoxBrowser extends AbstractBrowser<FirefoxDriver> {
    private static final Logger log = LoggerFactory.getLogger(FirefoxBrowser.class);

    //specify to output devtools console to stdout
    public static final String FIREFOX_WEBDRIVER_DEVTOOLS_CONSOLE_STDOUT = "webdriver.firefox.devtools.console.stdout";

    //specify regex to find console logs (error, warn, info, log, debug) and must include a group named "error" (default is "(?:console\.error|JavaScript error): (?<error>.*)")
    public static final String FIREFOX_WEBDRIVER_DEVTOOLS_CONSOLE_REGEX = "webdriver.firefox.devtools.console.regex";

    //specify path to firefox log file
    public static final String FIREFOX_WEBDRIVER_LOGFILE = "webdriver.firefox.logfile";

    //redirect console output to stdout (or file if used with option above)
    public static final String FIREFOX_DEVTOOLS_CONSOLE_STDOUT_CONTENT = "devtools.console.stdout.content";

    public static FirefoxDriver getDriver(final BrowserConfig browserConfig,
                                          final String browserPath,
                                          @Nullable final Capabilities capabilities) {
        return getDriver(FirefoxBrowser.class, browserConfig, browserPath, capabilities);
    }

    /**
     * Gets the default FirefoxDriver that tries to use the default system paths
     * for where the firefox binary should be
     *
     * @param capabilities optional desired capabilities
     * @return Default configured FirefoxDriver
     */
    @Override
    public FirefoxDriver getDriver(@Nullable final Capabilities capabilities) {
        final GeckoDriverService.Builder firefoxBuilder = new GeckoDriverService.Builder();
        setSystemProperties(firefoxBuilder);
        return constructFirefoxDriver(firefoxBuilder, null, capabilities);
    }

    /**
     * Configures a FirefoxDriver based on the browserConfig
     *
     * @param browserConfig browser config that points to the binary path of the browser and
     *                      optional profile
     * @param capabilities  optional desired capabilities
     * @return A configured FirefoxDriver based on the browserConfig passed in
     */
    @Override
    public FirefoxDriver getDriver(final BrowserConfig browserConfig, @Nullable final Capabilities capabilities) {
        final GeckoDriverService.Builder firefoxBuilder = new GeckoDriverService.Builder();

        if (browserConfig != null && browserConfig.getBinaryPath() != null) {
            firefoxBuilder.usingFirefoxBinary(new FirefoxBinary(new File(browserConfig.getBinaryPath())));
            FirefoxProfile profile = null;
            if (browserConfig.getProfilePath() != null) {
                final File profilePath = new File(browserConfig.getProfilePath());

                profile = new FirefoxProfile();
                addExtensionsToProfile(profile, profilePath);
                addPreferencesToProfile(profile, profilePath);
                addGeckoDriver(firefoxBuilder, profilePath);
            }

            setSystemProperties(firefoxBuilder);
            return constructFirefoxDriver(firefoxBuilder, profile, capabilities);
        }

        // Fall back on default firefox driver
        return getDriver(capabilities);
    }

    /**
     * Add command line options if provided via system property {@literal webdriver.firefox.switches}.
     */
    private static void addCommandLine(final FirefoxOptions options) {
        final String[] switches = StringUtils.split(WEBDRIVER_FIREFOX_SWITCHES.getSystemProperty(), ",");
        if (switches != null && switches.length > 0) {
            List<String> switchList = Arrays.asList(switches);
            log.info("Setting command line arguments for Firefox: " + switchList);
            options.addArguments(switchList);
        }
    }

    private static void addGeckoDriver(final GeckoDriverService.Builder firefoxBuilder, final File profilePath) {
        if (profilePath != null) {
            final File geckoDriverFile = new File(profilePath, "geckodriver" + (IS_OS_WINDOWS ? ".exe" : ""));
            if (geckoDriverFile.exists()) {
                firefoxBuilder.usingDriverExecutable(new File(geckoDriverFile.toString()));
            }
        }
    }

    static void addPreferencesToProfile(final FirefoxProfile profile, final File profilePath) {
        final File profilePreferencesFile = new File(profilePath, "profile.preferences");

        if (profilePreferencesFile.exists()) {
            final ProfilePreferences profilePreferences = new ProfilePreferences(profilePreferencesFile);
            profilePreferences.getPreferences().forEach((key, value) -> {
                if (value instanceof Integer) {
                    profile.setPreference(key, (Integer) value);
                } else if (value instanceof Boolean) {
                    profile.setPreference(key, (Boolean) value);
                } else {
                    profile.setPreference(key, (String) value);
                }
            });
        }
        addCustomProfilePreferences(profile);
    }

    static void addCustomRemotePreferences(FirefoxOptions options) {
        String remotePreferences = WebDriverProperties.WEBDRIVER_FIREFOX_REMOTE_PREFERENCES.getSystemProperty();
        if (remotePreferences != null) {
            log.info("Setting custom remote preferences: {}", remotePreferences);
            WebDriverUtil.setPropertiesFromString(remotePreferences, options, FirefoxBrowser::setRemotePreference);
        }
    }

    static void addCustomProfilePreferences(FirefoxProfile profile) {
        String profilePreferences = WebDriverProperties.WEBDRIVER_FIREFOX_PROFILE_PREFERENCES.getSystemProperty();
        if (profilePreferences != null) {
            log.info("Setting custom profile preferences: {}", profilePreferences);
            WebDriverUtil.setPropertiesFromString(profilePreferences, profile, FirefoxBrowser::setProfilePreference);
        }
    }

    private static void setRemotePreference(FirefoxOptions options, String key, String valStr) {
        try {
            int intValue = Integer.parseInt(valStr);
            options.addPreference(key, intValue);
        } catch (NumberFormatException e) {
            if (valStr.equalsIgnoreCase("true") || valStr.equalsIgnoreCase("false")) {
                options.addPreference(key, Boolean.parseBoolean(valStr));
            } else {
                options.addPreference(key, valStr);
            }
        }
    }

    private static void setProfilePreference(FirefoxProfile profile, String key, String valStr) {
        try {
            int intValue = Integer.parseInt(valStr);
            profile.setPreference(key, intValue);
        } catch (NumberFormatException e) {
            if (valStr.equalsIgnoreCase("true") || valStr.equalsIgnoreCase("false")) {
                profile.setPreference(key, Boolean.parseBoolean(valStr));
            } else {
                profile.setPreference(key, valStr);
            }
        }
    }

    private static void addExtensionsToProfile(final FirefoxProfile profile, final File profilePath) {
        // Filter the extensions path to only include extensions
        final File[] xpiFiles = profilePath.listFiles(file -> file.getName().matches(".*\\.xpi$"));
        if (xpiFiles != null) {
            for (File extension : xpiFiles) {
                profile.addExtension(extension);
            }
        }
    }

    /**
     * Gets a firefox driver based on the browser path based in
     *
     * @param browserPath  the path to the firefox binary to use for the firefox driver.
     * @param capabilities optional desired capabilities
     * @return A FirefoxDriver that is using the binary at the browserPath
     */
    public FirefoxDriver getDriver(final String browserPath, @Nullable final Capabilities capabilities) {
        if (browserPath != null) {
            final GeckoDriverService.Builder firefoxBuilder = new GeckoDriverService.Builder();
            firefoxBuilder.usingFirefoxBinary(new FirefoxBinary(new File(browserPath)));
            setSystemProperties(firefoxBuilder);
            return constructFirefoxDriver(firefoxBuilder, null, capabilities);
        }

        // Fall back on default firefox driver
        log.info("Browser path was null, falling back to default firefox driver.");
        return getDriver(capabilities);
    }

    public static void updateOptions(FirefoxOptions options) {
        options.setHeadless(isBrowserHeadless());
        // this is currently turned off as sometimes it causes console.log to be mixed with other JS errors
        options.addPreference(FIREFOX_DEVTOOLS_CONSOLE_STDOUT_CONTENT, Boolean.getBoolean(FIREFOX_WEBDRIVER_DEVTOOLS_CONSOLE_STDOUT));
        addCustomRemotePreferences(options);
    }


    private static FirefoxDriver constructFirefoxDriver(final GeckoDriverService.Builder firefoxBuilder,
                                                        FirefoxProfile profile,
                                                        @Nullable final Capabilities capabilities) {
        if (profile == null) {
            profile = new FirefoxProfile();
        }

        final FirefoxOptions options = capabilities != null ? new FirefoxOptions(capabilities) : new FirefoxOptions();
        options.setProfile(profile);
        addCommandLine(options);
        updateOptions(options);
        showEnv();

       return new FirefoxDriver(firefoxBuilder.build(), options);
    }

    private static void showEnv() {
        System.out.println("==== System properties ====");
        System.out.println(System.getProperties());
    }

    /**
     * Sets up system properties on the firefox driver.
     */
    private static void setSystemProperties(final GeckoDriverService.Builder builder) {
        System.out.println("==== Setting system properties ====");
        System.out.println("Display: " + System.getProperty("DISPLAY"));
        if (System.getProperty("DISPLAY") != null) {
            builder.withEnvironment(ImmutableMap.of("DISPLAY", System.getProperty("DISPLAY")));
        }
        WEBDRIVER_HTTP_FACTORY.setSystemProperty("netty");
        System.setProperty("webdriver.http.factory", "netty");
        setFirefoxWebdriverLogfile();
    }

    /**
     * Sets the system property for the webdriver.firefox.logfile and creates the file if it doesn't exist.
     */
    public static void setFirefoxWebdriverLogfile() {
        try {
            String systemPropertyFilePath = System.getProperty(FIREFOX_WEBDRIVER_LOGFILE, "");
            String logFilePath;
            if (!systemPropertyFilePath.isEmpty()) {
                File systemPropertyFile = new File(systemPropertyFilePath);
                if (!systemPropertyFile.exists()) {
                    systemPropertyFile.createNewFile();
                }
                logFilePath = systemPropertyFile.getAbsolutePath();
            } else {
                logFilePath = File.createTempFile("firefox-test-log", ".tmp").getAbsolutePath();
            }

            System.setProperty(FIREFOX_WEBDRIVER_LOGFILE, logFilePath);
        } catch (IOException e) {
            log.warn("Unable to create file for 'webdriver.firefox.logfile'. JS console error reporting will be unavailable.", e);
        }
    }
}
