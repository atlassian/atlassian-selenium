package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.webdriver.utils.WebDriverUtil;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.logging.Level;

import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_CAPABILITIES;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_LOGGING_BROWSER;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_LOGGING_CLIENT;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_LOGGING_DRIVER;

public class DesiredCapabilitiesFactory {
    private static final Logger log = LoggerFactory.getLogger(DesiredCapabilitiesFactory.class);

    /**
     * Return capabilities which specify a requested browser type.
     *
     * @param browserType type of browser requested
     * @return DesiredCapabilities capabilities object with a browser type specified
     * @throws UnsupportedOperationException if requested browser type is unsupported
     */
    @Nonnull
    public static DesiredCapabilities defaultCapabilitiesForBrowser(final Browser browserType)
            throws UnsupportedOperationException {
        final MutableCapabilities capabilities;
        switch (browserType) {
            case FIREFOX:
                capabilities = new FirefoxOptions();
                capabilities.setCapability("moz:firefoxOptions", "--enable-logging");
                break;
            case CHROME:
                capabilities = new ChromeOptions();
                break;
            case EDGE:
                capabilities = new EdgeOptions();
                break;
            default:
                log.error("Unknown browser: {}, defaulting to firefox.", browserType);
                capabilities = new FirefoxOptions();
                capabilities.setCapability("moz:firefoxOptions", "--enable-logging");
                capabilities.setCapability("goog:loggingPrefs", "browser:ALL");
        }

        return new DesiredCapabilities(capabilities);
    }

    /**
     * Parses a list of key=value pairs from "webdriver.capabilities" system property.
     *
     * @return DesiredCapabilities
     * @see WebDriverUtil#createCapabilitiesFromString(java.lang.String)
     */
    @Nonnull
    public static DesiredCapabilities customCapabilitiesFromSystemProperty() {
        final String capabilitiesStr = WEBDRIVER_CAPABILITIES.getSystemProperty();
        log.info("Loading custom capabilities " + capabilitiesStr);
        return WebDriverUtil.createCapabilitiesFromString(capabilitiesStr);
    }

    /**
     * Parses a set of webdriver.logging.* properties and sets logging levels for a browser, a client and a driver.
     *
     * @return DesiredCapabilities
     */
    @Nonnull
    public static DesiredCapabilities customCapabilitiesForLogging() {
        final DesiredCapabilities capabilities = new DesiredCapabilities();
        final LoggingPreferences loggingPreferences = loggingPreferencesFromProperties();
        setCapabilityIfNotNull(capabilities, CapabilityType.LOGGING_PREFS, loggingPreferences);
        return capabilities;
    }

    @Nullable
    private static LoggingPreferences loggingPreferencesFromProperties() {
        final LoggingPreferences loggingPreferences = new LoggingPreferences();
        enableLogTypeIfNonNull(loggingPreferences, LogType.BROWSER, WEBDRIVER_LOGGING_BROWSER.getSystemProperty());
        enableLogTypeIfNonNull(loggingPreferences, LogType.CLIENT, WEBDRIVER_LOGGING_CLIENT.getSystemProperty());
        enableLogTypeIfNonNull(loggingPreferences, LogType.DRIVER, WEBDRIVER_LOGGING_DRIVER.getSystemProperty());
        // return null if no logging preferences was set
        return loggingPreferences.getEnabledLogTypes().isEmpty() ? null : loggingPreferences;
    }

    private static void setCapabilityIfNotNull(final DesiredCapabilities capabilities,
                                               final String capabilityName,
                                               @Nullable final Object value) {
        if (value != null) {
            capabilities.setCapability(capabilityName, value);
        }
    }

    private static void enableLogTypeIfNonNull(final LoggingPreferences loggingPreferences,
                                               final String logType,
                                               @Nullable final String logLevel) {
        try {
            if (logLevel != null) {
                loggingPreferences.enable(logType, Level.parse(logLevel));
            }
        } catch (IllegalArgumentException ex) {
            log.warn("Invalid log level: {} for log type: {}", logLevel, logType);
        }
    }
}
