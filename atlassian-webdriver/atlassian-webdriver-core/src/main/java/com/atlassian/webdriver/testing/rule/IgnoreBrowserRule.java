package com.atlassian.webdriver.testing.rule;

import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.RequireBrowser;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import java.util.Collections;

import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assume.assumeThat;

/**
 * <p>
 * A rule that allows annotating test methods with the {@link com.atlassian.pageobjects.browser.IgnoreBrowser}
 * annotation and will skip the test if the current running driver
 * is in the list of browsers to ignore.
 * </p>
 *
 * <p>
 * Also skips tests annotated with {@link RequireBrowser} if they require a
 * particular browser that is not currently running.
 * </p>
 *
 * <p>
 * Requires surefire 2.7.2 or higher to show skipped tests in test results.
 * </p>
 *
 * @since 2.1
 */
public class IgnoreBrowserRule implements TestRule
{
    private static final Logger log = LoggerFactory.getLogger(IgnoreBrowserRule.class);

    private final Supplier<? extends Browser> currentBrowserSupplier;

    @Inject
    public IgnoreBrowserRule(Browser currentBrowser) {
        this(() -> requireNonNull(currentBrowser, "currentBrowser"));
    }

    public IgnoreBrowserRule(Supplier<? extends Browser> currentBrowserSupplier)
    {
        this.currentBrowserSupplier = requireNonNull(currentBrowserSupplier, "currentBrowserSupplier");
    }

    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                Class<?> clazz = description.getTestClass();
                Package pkg = clazz.getPackage();
                checkRequiredBrowsers(getRequired(description.getAnnotation(RequireBrowser.class)));
                checkRequiredBrowsers(getRequired(description.isSuite() ? null : clazz.getAnnotation(RequireBrowser.class)));
                checkRequiredBrowsers(getRequired(pkg.getAnnotation(RequireBrowser.class)));

                IgnoredBrowsers.fromAnnotation(description.getAnnotation(com.atlassian.pageobjects.browser.IgnoreBrowser.class))
                        .checkBrowser(currentBrowser(), description);
                IgnoredBrowsers.fromAnnotation(description.isSuite() ? null : clazz.getAnnotation(com.atlassian.pageobjects.browser.IgnoreBrowser.class))
                        .checkBrowser(currentBrowser(), description);
                base.evaluate();
            }

            private Iterable<Browser> getRequired(RequireBrowser requireBrowser) {
                if (requireBrowser != null) {
                    return ImmutableList.copyOf(requireBrowser.value());
                } else {
                    return Collections.emptyList();
                }
            }

            private void checkRequiredBrowsers(Iterable<Browser> required) {
                if (Iterables.isEmpty(required) || Iterables.contains(required, Browser.ALL)) {
                    return;
                }
                Browser latestBrowser = currentBrowser();
                if (!Iterables.contains(required, latestBrowser)) {
                    log.info(description.getDisplayName() + " ignored, since it requires one of " + required);
                    assumeThat(required, hasItem(latestBrowser));
                }
            }

            private Browser currentBrowser() {
                return currentBrowserSupplier.get();
            }
        };
    }

    private static final class IgnoredBrowsers {
        public static IgnoredBrowsers fromAnnotation(com.atlassian.pageobjects.browser.IgnoreBrowser ignoreBrowser) {
            if (ignoreBrowser == null) {
                return empty();
            } else {
                return new IgnoredBrowsers(ImmutableList.copyOf(ignoreBrowser.value()), ignoreBrowser.reason());
            }
        }

        public static IgnoredBrowsers empty() {
            return new IgnoredBrowsers(Collections.<Browser>emptyList(), "");
        }

        private final Iterable<Browser> ignored;
        private final String reason;

        private IgnoredBrowsers(Iterable<Browser> ignored, String reason) {
            this.ignored = ignored;
            this.reason = reason;
        }

        boolean isEmpty() {
            return Iterables.isEmpty(ignored);
        }

        void checkBrowser(Browser currentBrowser, Description description) {
            if (isEmpty()) {
                return;
            }
            for (Browser browser : ignored) {

                if (browser == currentBrowser || browser == Browser.ALL) {
                    log.info(description.getDisplayName() + " ignored, reason: " + reason);
                    assumeThat(browser, not(currentBrowser));
                    assumeThat(browser, not(Browser.ALL));
                }
            }
        }
    }
}
