package com.atlassian.webdriver.utils.element;

import com.atlassian.pageobjects.PageBinder;
import org.hamcrest.Matcher;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.Nonnull;
import jakarta.inject.Inject;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * <p>
 * A component that can be used to wait for certain conditions to happen on the tested page.
 * </p>
 *
 * <p>
 * The conditions are expressed as a generic {@link Function} from {@link WebDriver} to {@code boolean}.
 * {@link ElementConditions} contains factory methods to easily create some commonly used conditions.
 * </p>
 *
 * <p>
 * The {@link #DEFAULT_TIMEOUT} specify the default timeout used when no explicit timeout is provided by the client,
 * which is currently 30 seconds. Clients are encouraged to use their own timeout specific to the situation.
 * </p>
 *
 * <p>
 * NOTE: the default poll interval used by this class is as in the underlying {@link WebDriverWait} and is currently
 * 500ms (subject to change as the underlying {@link WebDriverWait} implementation changes. This may be generally
 * acceptable, but may not be granular enough for some scenarios (e.g. performance testing).
 * </p>
 *
 * Example usage:
 * <pre>
 *  <code>
 *      {@literal @Inject private WebDriverPoller poller; *
 *      // ...
 *      // wait for 5s for a 'my-element' to be present on the page
 *      poller.waitUntil(ElementConditions.isPresent(By.id("my-element")), Duration.ofSeconds(5));
 *      }
 *  </code>
 * </pre>
 *
 * <p>
 * As of 2.3, {@code WebDriverPoller} also supports waiting for {@code WebElement}-specific predicates and matchers,
 * which require the web element to be already located.
 * </p>
 *
 * <p>
 * This component can be injected into page objects running within a {@link PageBinder} context.
 * </p>
 *
 * <p>
 * For more sophisticated polling/waiting toolkit, check the {@code PageElement} API in the
 * atlassian-pageobjects-elements module.
 * </p>
 *
 * @since 2.2
 * @see ElementConditions
 * @see WebDriverWait
 */
public final class WebDriverPoller
{
    public static final Duration DEFAULT_TIMEOUT = Duration.ofSeconds(30);

    private final WebDriver webDriver;
    private final Duration timeout;

    @Inject
    public WebDriverPoller(@Nonnull WebDriver webDriver)
    {
        this(webDriver, DEFAULT_TIMEOUT);
    }

    /**
     * @deprecated since 4.0.0, use {@link #WebDriverPoller(WebDriver, Duration)} instead.
     */
    @Deprecated
    public WebDriverPoller(@Nonnull WebDriver webDriver, long timeout, @Nonnull TimeUnit timeUnit)
    {
        this(webDriver, Duration.ofNanos(timeUnit.toNanos(timeout)));
    }

    /**
     * @since 4.0.0
     */
    public WebDriverPoller(@Nonnull WebDriver webDriver, @Nonnull Duration timeout)
    {
        this.timeout = requireNonNull(timeout);

        checkArgument(!timeout.isNegative() && !timeout.isZero(), "Timeout must be positive.");

        this.webDriver = requireNonNull(webDriver, "webDriver");
    }

    /**
     * @deprecated since 4.0.0, use {@link #withDefaultTimeout(Duration)} instead.
     */
    @Deprecated
    @Nonnull
    public WebDriverPoller withDefaultTimeout(long timeout, @Nonnull TimeUnit timeUnit)
    {
        return new WebDriverPoller(webDriver, Duration.ofNanos(timeUnit.toNanos(timeout)));
    }

    /**
     * @since 4.0.0
     */
    @Nonnull
    public WebDriverPoller withDefaultTimeout(@Nonnull Duration timeout)
    {
        return new WebDriverPoller(webDriver, timeout);
    }

    /**
     * Wait until {@literal condition} is {@literal true}, up to the default timeout. The default timeout depends
     * on the arguments supplied while creating this {@code WebDriverPoller}.
     *
     * @param condition condition that must evaluate to {@literal true}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @see #DEFAULT_TIMEOUT
     */
    public void waitUntil(@Nonnull Function<WebDriver, Boolean> condition)
    {
        waitUntil(condition, timeout);
    }

    /**
     * Wait until {@literal condition} up to the {@literal timeoutInSeconds}.
     *
     * @param condition condition that must evaluate to {@literal true}
     * @param timeoutInSeconds timeout in seconds to wait for {@literal condition} to come {@code true}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @deprecated since 4.0.0, use {@link #waitUntil(Function, Duration)} instead.
     */
    @Deprecated
    public void waitUntil(@Nonnull Function<WebDriver, Boolean> condition, long timeoutInSeconds)
    {
        waitUntil(condition, Duration.ofSeconds(timeoutInSeconds));
    }

    /**
     * Wait until {@literal condition} up to the {@literal timeout} specified by {@literal unit}.
     *
     * @param condition condition that must evaluate to {@literal true}
     * @param timeout timeout to wait for {@literal condition} to come true
     * @param unit unit of the {@literal timeout}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @deprecated since 4.0.0, use {@link #waitUntil(Function, Duration)} instead.
     */
    @Deprecated
    public void waitUntil(@Nonnull Function<WebDriver, Boolean> condition, long timeout, @Nonnull TimeUnit unit)
    {
        waitUntil(condition, Duration.ofNanos(unit.toNanos(timeout)));
    }

    /**
     * Wait until {@literal condition} up to the {@literal timeout} specified by {@literal unit}.
     *
     * @param condition condition that must evaluate to {@literal true}
     * @param timeout timeout to wait for {@literal condition} to come true
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 4.0.0
     */
    public void waitUntil(@Nonnull Function<WebDriver, Boolean> condition, @Nonnull Duration timeout)
    {
        new WebDriverWait(webDriver, timeout)
                .until(condition);
    }

    /**
     * Wait until {@literal condition} is {@code true} for {@code element}, up to the default timeout. The default
     * timeout depends on the arguments supplied while creating this {@code WebDriverPoller}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@literal true}, expressed by a {@link Predicate}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 2.3
     *
     * @see #DEFAULT_TIMEOUT
     */
    public void waitUntil(@Nonnull WebElement element, @Nonnull Predicate<WebElement> condition)
    {
        waitUntil(element, condition, timeout);
    }

    /**
     * Wait until {@literal condition} is {@code true} for {@code element}, up to the {@literal timeoutInSeconds}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@literal true}, expressed by a {@link Predicate}
     * @param timeoutInSeconds timeout in seconds to wait for {@literal condition} to come {@code true}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 2.3
     * @deprecated since 4.0.0, use {@link #waitUntil(WebElement, Predicate, Duration)} instead.
     */
    @Deprecated
    public void waitUntil(@Nonnull WebElement element, @Nonnull Predicate<WebElement> condition, long timeoutInSeconds)
    {
        waitUntil(element, condition, Duration.ofSeconds(timeoutInSeconds));
    }

    /**
     * Wait until {@literal condition} is {@code true} for {@code element}, up to the {@literal timeout} specified
     * by {@literal unit}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@literal true}, expressed by a {@link Predicate}
     * @param timeout timeout to wait for {@literal condition} to come true
     * @param unit unit of the {@literal timeout}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 2.3
     * @deprecated since 4.0.0, use {@link #waitUntil(WebElement, Predicate, Duration)} instead.
     */
    @Deprecated
    public void waitUntil(@Nonnull WebElement element, @Nonnull Predicate<WebElement> condition,
                          long timeout, TimeUnit unit)
    {
        waitUntil(element, condition, Duration.ofNanos(unit.toNanos(timeout)));
    }

    /**
     * Wait until {@literal condition} is {@code true} for {@code element}, up to the {@literal timeout} specified
     * by {@literal unit}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@literal true}, expressed by a {@link Predicate}
     * @param timeout timeout to wait for {@literal condition} to come true
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 4.0.0
     */
    public void waitUntil(@Nonnull WebElement element, @Nonnull Predicate<WebElement> condition,
                          @Nonnull Duration timeout)
    {
        new FluentWait<>(requireNonNull(element, "element"))
                .withTimeout(timeout)
                .until(condition::test);
    }

    /**
     * Wait until {@literal condition} is {@code true} for {@code element}, up to the default timeout. The default
     * timeout depends on the arguments supplied while creating this {@code WebDriverPoller}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@code true}, expressed by a {@code Matcher}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 2.3
     *
     * @see #DEFAULT_TIMEOUT
     */
    public void waitUntil(@Nonnull WebElement element, @Nonnull Matcher<? super WebElement> condition)
    {
        waitUntil(element, condition, timeout);
    }

    /**
     * Wait until {@code condition} is {@code true} for {@code element}, up to the {@code timeoutInSeconds}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@code true}, expressed by a {@link Matcher}
     * @param timeoutInSeconds timeout in seconds to wait for {@code condition} to come {@code true}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 2.3
     * @deprecated since 4.0.0, use {@link #waitUntil(WebElement, Matcher, Duration)} instead.
     */
    @Deprecated
    public void waitUntil(@Nonnull WebElement element, @Nonnull Matcher<? super WebElement> condition,
                          long timeoutInSeconds)
    {
        waitUntil(element, condition, Duration.ofSeconds(timeoutInSeconds));
    }

    /**
     * Wait until {@literal condition} is {@code true} for {@code element}, up to the {@code timeout} specified
     * by {@code unit}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@code true}, expressed by a {@link Matcher}
     * @param timeout timeout to wait for {@code condition} to come true
     * @param unit unit of the {@code timeout}
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 2.3
     * @deprecated since 4.0.0, use {@link #waitUntil(WebElement, Matcher, Duration)} instead.
     */
    @Deprecated
    public void waitUntil(@Nonnull WebElement element, @Nonnull Matcher<? super WebElement> condition,
                          long timeout, TimeUnit unit)
    {
        waitUntil(element, newMatcherPredicate(condition), timeout, unit);
    }

    /**
     * Wait until {@literal condition} is {@code true} for {@code element}, up to the {@code timeout} specified
     * by {@code unit}.
     *
     * @param element the element to examine
     * @param condition condition that must evaluate to {@code true}, expressed by a {@link Matcher}
     * @param timeout timeout to wait for {@code condition} to come true
     * @throws TimeoutException if the condition does not come true before the timeout expires
     * @since 4.0.0
     */
    public void waitUntil(@Nonnull WebElement element, @Nonnull Matcher<? super WebElement> condition,
                          @Nonnull Duration timeout)
    {
        waitUntil(element, newMatcherPredicate(condition), timeout);
    }

    static <E> Predicate<E> newMatcherPredicate(final Matcher<? super E> filter)
    {
        return filter::matches;
    }
}
