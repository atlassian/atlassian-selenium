package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.webdriver.browsers.chrome.ChromeBrowser;
import com.atlassian.webdriver.browsers.firefox.FirefoxBrowser;
import com.google.common.annotations.VisibleForTesting;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handles connecting to a web driver server remotely.
 *
 * @since 2.1.0
 */
class RemoteWebDriverFactory {
    private static final Logger log = LoggerFactory.getLogger(RemoteWebDriverFactory.class);
    private static final Pattern remoteBrowserPathPattern = Pattern.compile("^([A-Za-z0-9_.-]+):url=(.*)$");

    public static boolean matches(String browserProperty) {
        return remoteBrowserPathPattern.matcher(browserProperty).matches();
    }

    public static Browser getBrowser(String browserProperty) {
        final Matcher matcher = remoteBrowserPathPattern.matcher(browserProperty);
        if (matcher.matches()) {
            return Browser.typeOf(matcher.group(1));
        }
        log.warn("Cannot find a url to connect to with a RemoteWebDriver. Falling back to using a local FirefoxDriver instead");
        return Browser.FIREFOX;
    }

    public static WebDriverContext getDriverContext(String browserProperty) {
        final Matcher matcher = remoteBrowserPathPattern.matcher(browserProperty);

        if (!matcher.matches()) {
            // this shouldn't happen anyway.
            log.warn("Cannot find a url to connect to with a RemoteWebDriver. Falling back to using a local FirefoxDriver instead");
            return new DefaultWebDriverContext(new FirefoxBrowser().getDriver(), Browser.FIREFOX);
        }

        final Browser browserType = Browser.typeOf(matcher.group(1));
        final String serverUrlString = matcher.group(2);
        final URL serverUrl = buildServerUrl(serverUrlString);

        final DesiredCapabilities capabilities = DesiredCapabilitiesFactory.defaultCapabilitiesForBrowser(browserType);
        final DesiredCapabilities customCapabilities = DesiredCapabilitiesFactory.customCapabilitiesFromSystemProperty();
        final DesiredCapabilities loggingCapabilities = DesiredCapabilitiesFactory.customCapabilitiesForLogging();

        capabilities.merge(getBrowserSpecificCapabilities(browserType));
        capabilities.merge(customCapabilities);
        capabilities.merge(loggingCapabilities);

        log.debug("Merged capabilities: {}", capabilities);

        RemoteWebDriver driver = new RemoteWebDriver(serverUrl, capabilities);
        driver.setFileDetector(new LocalFileDetector());

        return new DefaultWebDriverContext(driver, browserType);
    }

    @VisibleForTesting
    static URL buildServerUrl(final String serverUrlString) {
        try {
            final StringBuilder sb = new StringBuilder(serverUrlString);
            if (!serverUrlString.contains("wd/hub")) {
                if (!serverUrlString.endsWith("/")) {
                    sb.append("/");
                }
                sb.append("wd/hub");
            }

            return new URL(sb.toString());
        } catch (MalformedURLException e) {
            log.error("Invalid url provided: '{}', defaulting to http://localhost:4444/wd/hub", serverUrlString);
            return defaultServerUrl();
        }
    }

    @VisibleForTesting
    static URL defaultServerUrl() {
        try {
            return new URL("http://localhost:4444/wd/hub");
        } catch (MalformedURLException ex) {
            // this shouldn't happen, ignore
            return null;
        }
    }


    /**
     * Get capabilities from browser options (firefox or chrome) & update options (headless mode)
     *
     * @param browser browser type
     * @return browser capabilities
     */
    @VisibleForTesting
    static MutableCapabilities getBrowserSpecificCapabilities(Browser browser) {
        MutableCapabilities browserSpecificCapabilities = new MutableCapabilities();
        switch (browser) {
            default:
            case UNKNOWN:
                log.warn("Unknown browser: {}, cannot get browser specific capabilities.", browser);
                break;
            case CHROME:
                browserSpecificCapabilities = new ChromeOptions();
                ChromeBrowser.updateOptions((ChromeOptions) browserSpecificCapabilities);
                break;
            case FIREFOX:
                browserSpecificCapabilities = new FirefoxOptions();
                FirefoxBrowser.updateOptions((FirefoxOptions) browserSpecificCapabilities);
                FirefoxBrowser.setFirefoxWebdriverLogfile();
                break;
        }
        return browserSpecificCapabilities;
    }


    private RemoteWebDriverFactory() {
        throw new AssertionError("Don't instantiate. It has static methods only.");
    }
}
