package com.atlassian.webdriver.browsers.chrome;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.webdriver.browsers.AbstractBrowser;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;

import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_CHROME_SWITCHES;
import static com.atlassian.webdriver.WebDriverFactory.isBrowserHeadless;
import static org.apache.commons.lang3.SystemUtils.IS_OS_WINDOWS;

/**
 * @since 2.1
 */
public class ChromeBrowser extends AbstractBrowser<ChromeDriver> {
    private static final Logger log = LoggerFactory.getLogger(ChromeBrowser.class);

    public static ChromeDriver getDriver(final BrowserConfig browserConfig,
                                         final String browserPath,
                                         @Nullable final Capabilities capabilities) {
        return getDriver(ChromeBrowser.class, browserConfig, browserPath, capabilities);
    }

    @Override
    public ChromeDriver getDriver(@Nullable final Capabilities capabilities) {
        return new ChromeDriver(new ChromeOptions().merge(capabilities));
    }

    /**
     * Configures a ChromeDriver based on the browserConfig
     *
     * @param browserConfig browser config that points to the binary path of the browser and
     *                      optional profile
     * @return A configured ChromeDriver based on the browserConfig passed in
     */
    @Override
    public ChromeDriver getDriver(final BrowserConfig browserConfig,
                                  @Nullable final Capabilities capabilities) {
        if (browserConfig != null) {
            final ChromeOptions options = new ChromeOptions();
            options.merge(capabilities);
            options.setBinary(browserConfig.getBinaryPath());
            setDefaultArgs(options);
            addCommandLine(options);
            updateOptions(options);

            final ChromeDriverService.Builder chromeServiceBuilder = new ChromeDriverService.Builder();
            setChromeServicePath(browserConfig, chromeServiceBuilder);
            setEnvironment(chromeServiceBuilder);
            chromeServiceBuilder.usingAnyFreePort();
            ChromeDriverService chromeDriverService = chromeServiceBuilder.build();

            return new ChromeDriver(chromeDriverService, options);
        }

        // Fall back on default chrome driver
        return getDriver(capabilities);
    }

    /**
     * Gets a chrome driver based on the browser path based in
     *
     * @param browserPath the path to the chrome binary to use for the chrome driver.
     * @return A ChromeDriver that is using the binary at the browserPath
     */
    @Override
    public ChromeDriver getDriver(final String browserPath,
                                  @Nullable final Capabilities capabilities) {
        if (browserPath != null) {
            final ChromeOptions options = new ChromeOptions();
            options.merge(capabilities);
            options.setBinary(browserPath);
            updateOptions(options);

            return new ChromeDriver(options);
        } else {
            // Fall back on default chrome driver
            log.info("Browser path was null, falling back to default chrome driver.");
            return getDriver(capabilities);
        }
    }

    public static void updateOptions(ChromeOptions options) {
        options.setHeadless(isBrowserHeadless());
    }

    private static void setDefaultArgs(final ChromeOptions options) {
        // as of Chrome 30+ setuid based sandbox doesn't work on Linux due to permission issues
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("--disable-dev-shm-usage");
    }

    private static void setChromeServicePath(final BrowserConfig browserConfig,
                                             final ChromeDriverService.Builder chromeServiceBuilder) {
        if (browserConfig.getProfilePath() != null) {
            final File profilePath = new File(browserConfig.getProfilePath());
            final File chromeDriverFile = new File(profilePath, "chromedriver" + (IS_OS_WINDOWS ? ".exe" : ""));
            if (chromeDriverFile.exists()) {
                chromeServiceBuilder.usingDriverExecutable(chromeDriverFile);
            }
        }
    }

    /**
     * Add command line options if provided via system property {@literal webdriver.chrome.switches}.
     */
    private static void addCommandLine(final ChromeOptions options) {
        final String[] switches = StringUtils.split(WEBDRIVER_CHROME_SWITCHES.getSystemProperty(), ",");
        if (switches != null && switches.length > 0) {
            final List<String> switchList = Arrays.asList(switches);
            log.info("Setting command line arguments for Chrome: " + switchList);
            options.addArguments(switchList);
        }
    }

    /**
     * Sets up system properties on the chrome driver service.
     *
     * @param chromeDriverServiceBuilder the chrome driver service to set environment map on.
     */
    private static void setEnvironment(final ChromeDriverService.Builder chromeDriverServiceBuilder) {
        final Map<String, String> env = Maps.newHashMap();
        final String DISPLAY = "DISPLAY";
        if (System.getProperty(DISPLAY) != null) {
            env.put(DISPLAY, System.getProperty(DISPLAY));
        }
        chromeDriverServiceBuilder.withEnvironment(env);
    }

}
