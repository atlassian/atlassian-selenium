package com.atlassian.webdriver.pageobjects;

import com.atlassian.pageobjects.binder.PostInjectionProcessor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import jakarta.inject.Inject;

/**
 * Processor that will use {@link PageFactory} to initialize the object
 */
public class PageFactoryPostInjectionProcessor implements PostInjectionProcessor
{

    private final WebDriver webDriver;

    @Inject
    public PageFactoryPostInjectionProcessor(WebDriver webDriver)
    {
        this.webDriver = webDriver;
    }

    public <P> P process(P pageObject)
    {
        PageFactory.initElements(webDriver, pageObject);
        return pageObject;
    }
}
