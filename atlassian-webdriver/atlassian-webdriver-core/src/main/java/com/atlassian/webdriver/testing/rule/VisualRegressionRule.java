package com.atlassian.webdriver.testing.rule;

import com.atlassian.webdriver.debug.WebDriverDebug;
import org.apache.commons.io.FileUtils;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class VisualRegressionRule extends TestWatcher {
    private static final int DEFAULT_WAIT_MS = 1_000;
    private final Duration PAGE_LOAD_TIMEOUT_SEC = Duration.ofSeconds(5);
    private final Class testSuiteClass;
    private final WebDriver webDriver;
    private final WebDriverDebug webDriverDebug;
    private final String imageDir;
    private String filenamePrefix;
    private int counter = 0;


    public VisualRegressionRule(Class testSuiteClass, WebDriver webDriver) {
        // save to default directory unless specified
        this(testSuiteClass, webDriver, "target/vr-images");
    }

    public VisualRegressionRule(Class testSuiteClass, WebDriver webDriver, String imageDir) {
        this.testSuiteClass = testSuiteClass;
        this.imageDir = imageDir;
        this.webDriver = webDriver;
        this.webDriverDebug = new WebDriverDebug(webDriver);
    }

    @Override
    protected void starting(@Nonnull final Description d) {
        String sanitizedPackageClassName = this.testSuiteClass.getCanonicalName().replace(".", "/");
        filenamePrefix = String.format("%s/%s", sanitizedPackageClassName, d.getMethodName());
    }

    @Override
    protected void failed(@Nonnull final Throwable e, @Nonnull final Description d) {
        captureId("failed-test");
    }

    public void capture() {
        captureId(Integer.toString(counter++));
    }

    /**
     * Wait for 1000ms before capturing without argument.
     * Pass ms value as first argument to specify.
     */
    public void captureAfterMs() {
        captureAfterMs(DEFAULT_WAIT_MS);
    }

    public void captureAfterMs(int milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        capture();
    }

    /**
     * Capture with custom ID suffix filename rather than using auto-incrementing value
     */
    public void captureId(String id) {
        new WebDriverWait(webDriver, PAGE_LOAD_TIMEOUT_SEC).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));

        String filename = String.format("%s-%s.png", filenamePrefix, id);
        webDriverDebug.takeScreenshotTo(new File(imageDir, filename));
    }

    /**
     * Wait for 1000ms before capturing without argument.
     * Pass ms value as first argument to specify.
     */
    public void captureIdAfterMs(String id) {
        captureIdAfterMs(id, DEFAULT_WAIT_MS);
    }

    public void captureIdAfterMs(String id, int milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        captureId(id);
    }
}
