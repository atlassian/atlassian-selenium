package com.atlassian.webdriver.debug;

import com.atlassian.webdriver.utils.WebDriverUtil;
import com.google.common.collect.ImmutableList;
import org.openqa.selenium.UnsupportedCommandException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.StreamSupport;

/**
 * Default implementation of {@link JavaScriptErrorRetriever} - moved from {@link DefaultJavaScriptErrorRetriever}.
 *
 * @since 3.1.4
 */
public class GenericJavaScriptErrorRetriever implements JavaScriptErrorRetriever {
    private static final Logger log = LoggerFactory.getLogger(GenericJavaScriptErrorRetriever.class);

    private static final Map<SessionId, Boolean> errorRetrievalSupportedCache = new ConcurrentHashMap<>();

    private final Supplier<? extends WebDriver> webDriver;

    public GenericJavaScriptErrorRetriever(Supplier<? extends WebDriver> webDriver) {
        this.webDriver = webDriver;
    }

    @Override
    public boolean isErrorRetrievalSupported() {
        SessionId sessionId = WebDriverUtil.as(webDriver.get(), RemoteWebDriver.class).getSessionId();
        return errorRetrievalSupportedCache.computeIfAbsent(sessionId, ignored -> {
            try {
                return webDriver.get().manage().logs().getAvailableLogTypes().contains(LogType.BROWSER);
            } catch (UnsupportedCommandException e) {
                return false;
            } catch (WebDriverException e) {
                log.warn("WebDriverException during evaluation if 'error retrieval' is supported... probably buggy browser/driver", e);
                return false;
            }
        });
    }

    @Override
    public Iterable<JavaScriptErrorInfo> getErrors() {
        if (isErrorRetrievalSupported()) {
            return () -> StreamSupport.stream(webDriver.get().manage().logs().get(LogType.BROWSER).spliterator(), false)
                    .map(entry -> (JavaScriptErrorInfo) new JavaScriptErrorInfoImpl(entry.toString(), entry.getMessage())).iterator();
        } else {
            return ImmutableList.of();
        }
    }

}
