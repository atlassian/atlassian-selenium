package com.atlassian.webdriver;

import com.atlassian.annotations.Internal;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.BrowserAware;
import org.openqa.selenium.WebDriver;

/**
 * Context object representing both a running {@link Browser browser instance} and a configured {@link WebDriver} instance
 * to interact with the browser.
 *
 * @since 3.0
 */
@Internal
public interface WebDriverContext extends BrowserAware
{
    Browser getBrowser();

    WebDriver getDriver();
}
