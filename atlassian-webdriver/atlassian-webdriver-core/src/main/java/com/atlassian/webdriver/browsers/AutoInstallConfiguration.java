package com.atlassian.webdriver.browsers;

import com.atlassian.browsers.AbstractInstallConfigurator;
import com.atlassian.browsers.BrowserAutoInstaller;
import com.atlassian.browsers.BrowserConfig;
import com.atlassian.browsers.BrowserConfiguration;

import javax.annotation.Nonnull;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_CHROME_BIN;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_FIREFOX_BIN;

/**
 * @since 2.0
 */
public class AutoInstallConfiguration {
    /**
     * Setup a browser using system props to determine which, using Maven's target dir as a temp dir.
     */
    public static BrowserConfig setupBrowser() {
        return setupBrowser(new WebDriverBrowserConfiguration());
    }

    /**
     * Setup a browser specifying the browser and temp dir.
     */
    public static BrowserConfig setupBrowser(final BrowserConfiguration browserConfiguration) {
        final AtomicReference<BrowserConfig> ref = new AtomicReference<>();
        final BrowserAutoInstaller browserAutoInstaller = new BrowserAutoInstaller(browserConfiguration, new AbstractInstallConfigurator() {
            @Override
            public void setupBrowser(@Nonnull BrowserConfig browserConfig) {
                ref.set(browserConfig);
                switch (browserConfig.getBrowserType()) {
                    case FIREFOX:
                        WEBDRIVER_FIREFOX_BIN.setSystemProperty(browserConfig.getBinaryPath());
                        break;
                    case CHROME:
                        WEBDRIVER_CHROME_BIN.setSystemProperty(browserConfig.getBinaryPath());
                        break;
                    default:
                        // we don't handle a path to a binary for other types
                }
            }
        });
        browserAutoInstaller.setupBrowser();
        return ref.get();
    }

    private AutoInstallConfiguration() {
        throw new AssertionError("Don't instantiate. It has static methods only.");
    }

}