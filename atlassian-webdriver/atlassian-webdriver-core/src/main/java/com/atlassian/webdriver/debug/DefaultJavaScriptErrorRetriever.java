package com.atlassian.webdriver.debug;

import com.google.common.collect.ImmutableList;
import org.openqa.selenium.WebDriver;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Supplier;

import static com.atlassian.webdriver.debug.FirefoxLogParsingJavaScriptErrorRetriever.getFilePathFromSystemProperty;
import static com.atlassian.webdriver.debug.FirefoxLogParsingJavaScriptErrorRetriever.getLogRegexPatternFromSystemProperty;

/**
 * Default implementation of {@link JavaScriptErrorRetriever}.
 * Original implementation has been moved to {@link GenericJavaScriptErrorRetriever}.
 * This one iterates over list of delegates to handle JS errors.
 *
 * @since 2.3
 */
public class DefaultJavaScriptErrorRetriever implements JavaScriptErrorRetriever {
    private final Collection<JavaScriptErrorRetriever> errorRetrievers;

    public DefaultJavaScriptErrorRetriever(Supplier<? extends WebDriver> webDriver) {
        this.errorRetrievers = ImmutableList.of(
                new FirefoxLogParsingJavaScriptErrorRetriever(getFilePathFromSystemProperty(), getLogRegexPatternFromSystemProperty()),
                new GenericJavaScriptErrorRetriever(webDriver)
        );
    }

    @Override
    public boolean isErrorRetrievalSupported() {
        return this.errorRetrievers.stream().anyMatch(JavaScriptErrorRetriever::isErrorRetrievalSupported);
    }

    @Override
    public Iterable<JavaScriptErrorInfo> getErrors() {
        return this.errorRetrievers.stream()
                .filter(JavaScriptErrorRetriever::isErrorRetrievalSupported)
                .findFirst()
                .map(JavaScriptErrorRetriever::getErrors)
                .orElse(Collections.emptyList());
    }
}
