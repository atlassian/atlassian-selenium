package com.atlassian.webdriver.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * JavaScript Utilities for executing specific javascript events.
 */
public class JavaScriptUtils
{

    private JavaScriptUtils() {}


    public static void dispatchEvent(String eventType, WebElement element, WebDriver webDriver)
    {
        execute("arguments[0].dispatchEvent(typeof (Event) === 'function' ? new Event(arguments[1]) : (function (ev) {\n" +
                "    var tmp = document.createEvent('Event');\n" +
                "    tmp.initEvent(ev, true, true);\n" +
                "    return tmp;\n" +
                "})(arguments[1]));", webDriver, element, eventType);
    }

    /**
     * Dispatches a javascript mouse event in the browser on a specified element
     *
     * @param event The name of the event to dispatch. eg. load.
     * @param el The element to fire the event on.
     * @param driver the webdriver instance that executes the javascript event.
     */
    public static void dispatchMouseEvent(String event, WebElement el, WebDriver driver)
    {
        dispatchEvent(event, el, driver);
    }

    @SuppressWarnings("unchecked")
    public static <T> T execute(String js, WebDriver driver, Object... arguments)
    {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        return (T) jsExecutor.executeScript(js, arguments);
    }


    public static <T> T execute(Class<T> expectedReturn, String js, WebDriver driver, Object... arguments)
    {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        final Object result = jsExecutor.executeScript(js, arguments);
        if (result != null && !expectedReturn.isInstance(result))
        {
            throw new ClassCastException("Expected result type " + expectedReturn.getName() + " but was: "
                    + result.getClass().getName());
        }
        return expectedReturn.cast(result);
    }
}