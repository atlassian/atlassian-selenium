package com.atlassian.webdriver.testing.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a test class which is capturing visual regression screenshots
 * using {@link com.atlassian.webdriver.testing.rule.VisualRegressionRule}
 * in order to filter out visual regression tests via a test runner configuration
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface VisualRegressionTestClass {
}