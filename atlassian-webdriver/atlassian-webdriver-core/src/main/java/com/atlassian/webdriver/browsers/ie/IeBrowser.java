package com.atlassian.webdriver.browsers.ie;

import com.atlassian.browsers.BrowserConfig;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;

public final class IeBrowser {
    private static final Logger log = LoggerFactory.getLogger(IeBrowser.class);

    public static final String IE_SERVICE_EXECUTABLE = "IEDriverServer.exe";

    private IeBrowser() {
        throw new AssertionError("Don't instantiate me");
    }

    public static InternetExplorerDriver createIeDriver(@Nullable String browserPath, @Nullable BrowserConfig config) {
        return createIeDriver(browserPath, config, null);
    }

    public static InternetExplorerDriver createIeDriver(@Nullable final String browserPath,
                                                        @Nullable final BrowserConfig config,
                                                        @Nullable final Capabilities capabilities) {
        if (noBrowserPath(browserPath) && noServicePath(config)) {
            return createDefaultDriver();
        } else {
            final InternetExplorerDriverService service = setBrowserExecutablePath(browserPath,
                    setServiceExecutablePath(config, new InternetExplorerDriverService.Builder()))
                    .usingAnyFreePort()
                    .build();
            return new InternetExplorerDriver(service, new InternetExplorerOptions(capabilities));
        }
    }

    public static InternetExplorerDriver createDefaultDriver() {
        return new InternetExplorerDriver();
    }

    private static boolean noBrowserPath(@Nullable final String browserPath) {
        return browserPath == null;
    }

    private static boolean hasServicePath(@Nullable final BrowserConfig config) {
        return config != null && config.getProfilePath() != null;
    }

    private static boolean noServicePath(@Nullable final BrowserConfig config) {
        return !hasServicePath(config);
    }

    private static InternetExplorerDriverService.Builder setServiceExecutablePath(final BrowserConfig browserConfig,
                                                                                  final InternetExplorerDriverService.Builder builder) {
        if (hasServicePath(browserConfig)) {
            File profilePath = new File(browserConfig.getProfilePath());
            File ieDriverFile = new File(profilePath, IE_SERVICE_EXECUTABLE);
            if (ieDriverFile.isFile()) {
                builder.usingDriverExecutable(ieDriverFile);
            }
        }
        return builder;
    }

    private static InternetExplorerDriverService.Builder setBrowserExecutablePath(final String browserPath,
                                                                                  final InternetExplorerDriverService.Builder builder) {
        if (browserPath != null) {
            // can't do much here, IE driver knows better where to look for IE
            log.warn("Non-null browser path configured for IE: '{}', but IEDriver does not support custom browser paths", browserPath);
        }
        return builder;
    }

}
