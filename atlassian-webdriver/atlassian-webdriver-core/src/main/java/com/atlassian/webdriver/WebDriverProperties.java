package com.atlassian.webdriver;

public enum WebDriverProperties {
    WEBDRIVER_BROWSER("webdriver.browser"),
    WEBDRIVER_BROWSER_HEADLESS("webdriver.browser.headless"),
    WEBDRIVER_INIT_RETRY("webdriver.init.retry"),
    WEBDRIVER_CAPABILITIES("webdriver.capabilities"),
    WEBDRIVER_HTTP_FACTORY("webdriver.http.factory"),

    WEBDRIVER_LOGGING_DRIVER("webdriver.logging.driver"),
    WEBDRIVER_LOGGING_BROWSER("webdriver.logging.browser"),
    WEBDRIVER_LOGGING_CLIENT("webdriver.logging.client"),

    WEBDRIVER_FIREFOX_BIN("webdriver.firefox.bin"),
    WEBDRIVER_FIREFOX_SWITCHES("webdriver.firefox.switches"),
    WEBDRIVER_FIREFOX_PROFILE_PREFERENCES("webdriver.firefox.profile.preferences"),
    WEBDRIVER_FIREFOX_REMOTE_PREFERENCES("webdriver.firefox.remote.preferences"),

    WEBDRIVER_CHROME_BIN("webdriver.chrome.bin"),
    WEBDRIVER_CHROME_SWITCHES("webdriver.chrome.switches");

    private final String propertyName;

    WebDriverProperties(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getSystemProperty() {
        return System.getProperty(propertyName);
    }

    public String getSystemProperty(String defaultValue) {
        return System.getProperty(propertyName, defaultValue);
    }

    public Integer getSystemPropertyAsInt(int defaultValue) {
        return Integer.getInteger(propertyName, defaultValue);
    }

    public Boolean getSystemPropertyAsBool() {
        return Boolean.getBoolean(propertyName);
    }

    public void setSystemProperty(String value) {
        System.setProperty(propertyName, value);
    }

    public String getPropertyName() {
        return propertyName;
    }
}
