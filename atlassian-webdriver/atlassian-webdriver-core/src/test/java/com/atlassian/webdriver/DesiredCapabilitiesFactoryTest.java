package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;

import java.util.logging.Level;

import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_CAPABILITIES;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_LOGGING_BROWSER;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_LOGGING_CLIENT;
import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_LOGGING_DRIVER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.fail;

public class DesiredCapabilitiesFactoryTest {

    @After
    public void tearDown() {
        System.clearProperty(WEBDRIVER_CAPABILITIES.getPropertyName());
        System.clearProperty(WEBDRIVER_LOGGING_BROWSER.getPropertyName());
        System.clearProperty(WEBDRIVER_LOGGING_DRIVER.getPropertyName());
        System.clearProperty(WEBDRIVER_LOGGING_CLIENT.getPropertyName());
    }

    @Test
    public void defaultCapabilitiesForBrowserReturnsCapabilitiesForKnownBrowsers() {
        assertCapability(Browser.FIREFOX, null, "firefox");
        assertCapability(Browser.CHROME, null, "chrome");
        assertCapability(Browser.EDGE, null, "MicrosoftEdge");
    }

    private void assertCapability(Browser browserType, Platform platform, String browserName) {
        final Capabilities capabilities = DesiredCapabilitiesFactory.defaultCapabilitiesForBrowser(browserType);
        assertThat(capabilities.getBrowserName(), is(browserName));
        assertThat(capabilities.getBrowserVersion(), is(""));
        assertThat(capabilities.getPlatformName(), is(platform));
    }

    @Test
    public void customCapabilitiesFromSystemPropertyIsEmpty() {
        System.clearProperty(WEBDRIVER_CAPABILITIES.getPropertyName());
        final Capabilities capabilities = DesiredCapabilitiesFactory.customCapabilitiesFromSystemProperty();

        assertThat(capabilities.asMap().keySet(), is(empty()));
    }

    @Test
    public void customCapabilitiesFromSystemPropertyWithData() {
        WEBDRIVER_CAPABILITIES.setSystemProperty("abc=1;def=2");
        final Capabilities capabilities = DesiredCapabilitiesFactory.customCapabilitiesFromSystemProperty();

        assertThat(capabilities.getCapability("abc"), is("1"));
        assertThat(capabilities.getCapability("def"), is("2"));
    }

    @Test
    public void customCapabilitiesForLoggingIsEmptyWhenNoLoggingProperties() {
        System.clearProperty(WEBDRIVER_LOGGING_BROWSER.getPropertyName());
        System.clearProperty(WEBDRIVER_LOGGING_DRIVER.getPropertyName());
        System.clearProperty(WEBDRIVER_LOGGING_CLIENT.getPropertyName());
        final Capabilities capabilities = DesiredCapabilitiesFactory.customCapabilitiesForLogging();

        assertThat(capabilities.asMap().keySet(), is(empty()));
    }

    @Test
    public void customCapabilitiesForLoggingHasLoggingPropertiesObject() {
        System.setProperty(WEBDRIVER_LOGGING_BROWSER.getPropertyName(), "WARNING");
        System.setProperty(WEBDRIVER_LOGGING_DRIVER.getPropertyName(), "FINER");
        System.setProperty(WEBDRIVER_LOGGING_CLIENT.getPropertyName(), "INVALID_LEVEL");
        final Capabilities capabilities = DesiredCapabilitiesFactory.customCapabilitiesForLogging();

        // only one capability - the logging preferences object
        assertThat(capabilities.asMap().keySet(), hasSize(1));

        // the logging preferences has logging levels set for 2 components
        final Object loggingPreferencesObject = capabilities.getCapability(CapabilityType.LOGGING_PREFS);
        assertThat(loggingPreferencesObject, notNullValue());
        assertThat(loggingPreferencesObject, instanceOf(LoggingPreferences.class));

        final LoggingPreferences loggingPreferences = (LoggingPreferences) loggingPreferencesObject;
        assertThat(loggingPreferences.getEnabledLogTypes(), hasSize(2));
        assertThat(loggingPreferences.getLevel("browser"), is(Level.WARNING));
        assertThat(loggingPreferences.getLevel("driver"), is(Level.FINER));
    }

}
