package com.atlassian.webdriver.browsers.firefox;

import com.atlassian.webdriver.WebDriverProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.openqa.selenium.firefox.FirefoxOptions.FIREFOX_OPTIONS;

@RunWith(MockitoJUnitRunner.class)
public class FirefoxBrowserTest {

    @Mock
    private FirefoxProfile profileMock;

    @Test
    public void shouldAddPreferencesToProfileFromFile() throws IOException, URISyntaxException {
        Path sourcePath = Paths.get(getClass().getResource("/profile.preferences").toURI());
        FirefoxBrowser.addPreferencesToProfile(profileMock, sourcePath.getParent().toFile());

        verify(profileMock).setPreference("browser.startup.homepage", "https://www.example.com");
        verify(profileMock).setPreference("browser.cache.disk.enable", false);
    }

    @Test
    public void shouldAddPreferencesToProfileFromSystemProperty() {
        String systemPropertyPrefs = "browser.download.folderList=2;browser.download.manager.showWhenStarting=false";
        System.setProperty(WebDriverProperties.WEBDRIVER_FIREFOX_PROFILE_PREFERENCES.getPropertyName(), systemPropertyPrefs);

        FirefoxBrowser.addPreferencesToProfile(profileMock, null);

        verify(profileMock).setPreference("browser.download.folderList", 2);
        verify(profileMock).setPreference("browser.download.manager.showWhenStarting", false);
    }

    @Test
    public void updateOptionsShouldIncludeRemotePreferencesIfSetAsSystemProp() {
        String systemPropertyPrefs = "layout.testing.overlay-scrollbars.always-visible=true;some.int.pref=2;some.string.pref=value";
        System.setProperty(WebDriverProperties.WEBDRIVER_FIREFOX_REMOTE_PREFERENCES.getPropertyName(), systemPropertyPrefs);

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        FirefoxBrowser.updateOptions(firefoxOptions);

        Map prefsMap = (Map) ((Map)firefoxOptions.asMap().get(FIREFOX_OPTIONS)).get("prefs");

        assertEquals(prefsMap.get("layout.testing.overlay-scrollbars.always-visible"), true);
        assertEquals(prefsMap.get("some.int.pref"), 2);
        assertEquals(prefsMap.get("some.string.pref"), "value");
        assertNull(prefsMap.get("some.arbitrary.pref.that.was.not.set.as.system.prop"));
    }

    @Test
    public void updateOptionsShouldNotIncludeRemotePreferencesIfNotSetAsSystemProp() {
        System.clearProperty(WebDriverProperties.WEBDRIVER_FIREFOX_REMOTE_PREFERENCES.getPropertyName());

        FirefoxOptions firefoxOptions = new FirefoxOptions();
        FirefoxBrowser.updateOptions(firefoxOptions);

        Map prefsMap = (Map) ((Map)firefoxOptions.asMap().get(FIREFOX_OPTIONS)).get("prefs");

        assertNull(prefsMap.get("layout.testing.overlay-scrollbars.always-visible"));
    }
}
