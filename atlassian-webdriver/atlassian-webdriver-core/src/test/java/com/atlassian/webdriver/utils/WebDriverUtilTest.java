package com.atlassian.webdriver.utils;

import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class WebDriverUtilTest
{
    @Test
    public void testCreateCapabilitiesFromStringOneItem() throws Exception
    {
        assertEquals("osx", WebDriverUtil.createCapabilitiesFromString("so=osx").getCapability("so"));
    }

    @Test
    public void testCreateCapabilitiesFromStringMoreThanOneItem() throws Exception
    {
        DesiredCapabilities capabilities = WebDriverUtil.createCapabilitiesFromString("so=osx;browser=safari");
        assertEquals("osx", capabilities.getCapability("so"));
        assertEquals("safari", capabilities.getCapability("browser"));
    }

    @Test
    public void testCreateCapabilitiesFromStringDuplicatedItems() throws Exception
    {
        DesiredCapabilities capabilities = WebDriverUtil.createCapabilitiesFromString("so=osx;browser=safari;browser=firefox");
        assertEquals("osx", capabilities.getCapability("so"));
        assertEquals("firefox", capabilities.getCapability("browser")); // it will pick up the latest
    }

    @Test
    public void testCreateCapabilitiesFromNullString() throws Exception
    {
        DesiredCapabilities capabilities = WebDriverUtil.createCapabilitiesFromString(null);
        assertEquals(0, capabilities.asMap().size());
    }

    @Test
    public void testCreateCapabilitiesFromEmptyString() throws Exception
    {
        DesiredCapabilities capabilities = WebDriverUtil.createCapabilitiesFromString("");
        assertEquals(0, capabilities.asMap().size());
    }

    @Test
    public void testSetPropertiesFromString() {
        String capabilitiesString = "browserName=chrome;version=latest;platform=WINDOWS;headless=true";
        Map<String, Object> propMap = new HashMap<>();
        WebDriverUtil.setPropertiesFromString(capabilitiesString, propMap,
                (caps, key, val) -> propMap.put(key, val));
        assertEquals("chrome", propMap.get("browserName"));
        assertEquals("latest", propMap.get("version"));
        assertEquals("WINDOWS", propMap.get("platform"));
        assertEquals("true", propMap.get("headless"));
    }

}
