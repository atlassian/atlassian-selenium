package com.atlassian.webdriver.browsers;

import com.atlassian.browsers.BrowserConfig;
import com.atlassian.browsers.BrowserType;
import org.junit.Test;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.annotation.Nullable;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.fail;

public class AbstractBrowserTest {

    private static class MyBrowser extends AbstractBrowser<RemoteWebDriver> {
        public static final List<String> methodCalls = new ArrayList<>();

        public MyBrowser() {

        }

        @Override
        public RemoteWebDriver getDriver(@Nullable Capabilities capabilities) {
            methodCalls.add("getDriver(Capabilities)");
            return null;
        }

        @Override
        public RemoteWebDriver getDriver(BrowserConfig browserConfig, @Nullable Capabilities capabilities) {
            methodCalls.add("getDriver(BrowserConfig,Capabilities)");
            return null;
        }

        @Override
        public RemoteWebDriver getDriver(String browserPath, @Nullable Capabilities capabilities) {
            methodCalls.add("getDriver(String,Capabilities)");
            return null;
        }
    }

    @Test
    public void checkGetDriverPassesCallsWithNullCapabilities() {
        final AbstractBrowser<RemoteWebDriver> myBrowser = new MyBrowser();

        assertMethodCalls(myBrowser::getDriver,
                "getDriver(Capabilities)");

        assertMethodCalls(() -> myBrowser.getDriver((BrowserConfig) null),
                "getDriver(BrowserConfig,Capabilities)");

        assertMethodCalls(() -> myBrowser.getDriver((String) null),
                "getDriver(String,Capabilities)");
    }

    @Test
    public void checkStaticGetDriverCallProperGetDriverMethods() {
        assertMethodCalls(() -> MyBrowser.getDriver(MyBrowser.class, newBrowserConfig(), null, null),
                "getDriver(BrowserConfig,Capabilities)");

        assertMethodCalls(() -> MyBrowser.getDriver(MyBrowser.class, null, "", null),
                "getDriver(String,Capabilities)");

        assertMethodCalls(() -> MyBrowser.getDriver(MyBrowser.class, null, null, new DesiredCapabilities()),
                "getDriver(Capabilities)");
    }

    private void assertMethodCalls(final Runnable action, final String... methodsCalled) {
        MyBrowser.methodCalls.clear();
        action.run();
        assertThat(MyBrowser.methodCalls, hasItems(methodsCalled));
    }

    /**
     * A workaround for non-public BrowserConfig.
     */
    private BrowserConfig newBrowserConfig() {
        try {
            final Constructor<BrowserConfig> constructor = BrowserConfig.class
                    .getDeclaredConstructor(File.class, File.class, File.class, BrowserType.class);
            constructor.setAccessible(true);
            return constructor.newInstance(null, null, null, null);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            fail("Test setup failed " + ex.getMessage());
            return null;
        }
    }
}
