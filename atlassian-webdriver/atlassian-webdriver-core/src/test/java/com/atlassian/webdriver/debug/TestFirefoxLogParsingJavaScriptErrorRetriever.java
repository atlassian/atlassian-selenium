package com.atlassian.webdriver.debug;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.regex.Pattern;

import static com.atlassian.webdriver.debug.FirefoxLogParsingJavaScriptErrorRetriever.getLogRegexPatternFromSystemProperty;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;


@RunWith(MockitoJUnitRunner.Silent.class)
public class TestFirefoxLogParsingJavaScriptErrorRetriever {

    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    private FirefoxLogParsingJavaScriptErrorRetriever subject;

    @Test
    public void shouldParseErrorFromLogFile() throws URISyntaxException {
        subject = new FirefoxLogParsingJavaScriptErrorRetriever(Paths.get(getFilePath()).toString());

        final Iterable<JavaScriptErrorInfo> errors = subject.getErrors();

        Assert.assertThat(errors, contains(
                new JavaScriptErrorInfoImpl(
                        "http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/4.3.9-SNAPSHOT/_/download/resources/com.atlassian.jira.plugins.jira-wiki-editor:wiki-editor-resources/UndoManager.js?locale=en-PL, line 11: TypeError: test is undefined and other error: ble ble",
                        "http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/4.3.9-SNAPSHOT/_/download/resources/com.atlassian.jira.plugins.jira-wiki-editor:wiki-editor-resources/UndoManager.js?locale=en-PL, line 11: TypeError: test is undefined and other error: ble ble"
                ),
                new JavaScriptErrorInfoImpl(
                        "some other console error: error e",
                        "some other console error: error e"
                )
        ));
    }

    @Test
    public void shouldParseErrorFromLogFileWithDefaultRegex() throws URISyntaxException {
        subject = new FirefoxLogParsingJavaScriptErrorRetriever(Paths.get(getFilePath()).toString());

        final Iterable<JavaScriptErrorInfo> errors = subject.getErrors();

        Assert.assertThat(errors, contains(
                new JavaScriptErrorInfoImpl(
                        "http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/4.3.9-SNAPSHOT/_/download/resources/com.atlassian.jira.plugins.jira-wiki-editor:wiki-editor-resources/UndoManager.js?locale=en-PL, line 11: TypeError: test is undefined and other error: ble ble",
                        "http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/4.3.9-SNAPSHOT/_/download/resources/com.atlassian.jira.plugins.jira-wiki-editor:wiki-editor-resources/UndoManager.js?locale=en-PL, line 11: TypeError: test is undefined and other error: ble ble"
                ),
                new JavaScriptErrorInfoImpl(
                        "some other console error: error e",
                        "some other console error: error e"
                )
        ));
    }

    @Test
    public void shouldParseWarnFromLogFileWithRegex() throws URISyntaxException {
        subject = new FirefoxLogParsingJavaScriptErrorRetriever(Paths.get(getFilePath()).toString(), Pattern.compile("(?:console\\.warn|JavaScript warning): (?<error>.*)"));

        final Iterable<JavaScriptErrorInfo> errors = subject.getErrors();

        Assert.assertThat(errors, contains(
                new JavaScriptErrorInfoImpl("\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n attachDialog@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/1.0/_/download/resources/jira.webresources:project-type-warning/static/projecttypes/warning/dialog/project-type-warning-dialog.js?locale=en-PL:10:21\"",
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n attachDialog@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/1.0/_/download/resources/jira.webresources:project-type-warning/static/projecttypes/warning/dialog/project-type-warning-dialog.js?locale=en-PL:10:21\""),
                new JavaScriptErrorInfoImpl(
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n initialize@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/6.0.7/_/download/resources/com.atlassian.jira.jira-projects-plugin:sidebar-project-shortcuts/sidebar-project-shortcuts-Add.js?locale=en-PL:38:27\"",
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n initialize@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/6.0.7/_/download/resources/com.atlassian.jira.jira-projects-plugin:sidebar-project-shortcuts/sidebar-project-shortcuts-Add.js?locale=en-PL:38:27\""
                )
        ));
    }


    @Test
    public void shouldParseWarnFromLogFileWithRegexAndRegexGroupTwo() throws URISyntaxException {
        subject = new FirefoxLogParsingJavaScriptErrorRetriever(Paths.get(getFilePath()).toString(), Pattern.compile("(?:console\\.warn|JavaScript warning): (?<error>.*)"));

        final Iterable<JavaScriptErrorInfo> errors = subject.getErrors();

        Assert.assertThat(errors, contains(
                new JavaScriptErrorInfoImpl("\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n attachDialog@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/1.0/_/download/resources/jira.webresources:project-type-warning/static/projecttypes/warning/dialog/project-type-warning-dialog.js?locale=en-PL:10:21\"",
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n attachDialog@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/1.0/_/download/resources/jira.webresources:project-type-warning/static/projecttypes/warning/dialog/project-type-warning-dialog.js?locale=en-PL:10:21\""),
                new JavaScriptErrorInfoImpl(
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n initialize@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/6.0.7/_/download/resources/com.atlassian.jira.jira-projects-plugin:sidebar-project-shortcuts/sidebar-project-shortcuts-Add.js?locale=en-PL:38:27\"",
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n initialize@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/6.0.7/_/download/resources/com.atlassian.jira.jira-projects-plugin:sidebar-project-shortcuts/sidebar-project-shortcuts-Add.js?locale=en-PL:38:27\""
                )
        ));
    }

    @Test
    public void shouldParseWarnFromLogFileWithRegexFromSystemProperty() throws URISyntaxException {
        // Set system property so that it will be used in the constructor
        System.setProperty("webdriver.firefox.devtools.console.regex", "(?:console\\.warn|JavaScript warning): (?<error>.*)");

        subject = new FirefoxLogParsingJavaScriptErrorRetriever(Paths.get(getFilePath()).toString(), getLogRegexPatternFromSystemProperty());

        final Iterable<JavaScriptErrorInfo> errors = subject.getErrors();

        Assert.assertThat(errors, contains(
                new JavaScriptErrorInfoImpl("\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n attachDialog@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/1.0/_/download/resources/jira.webresources:project-type-warning/static/projecttypes/warning/dialog/project-type-warning-dialog.js?locale=en-PL:10:21\"",
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n attachDialog@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/1.0/_/download/resources/jira.webresources:project-type-warning/static/projecttypes/warning/dialog/project-type-warning-dialog.js?locale=en-PL:10:21\""),
                new JavaScriptErrorInfoImpl(
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n initialize@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/6.0.7/_/download/resources/com.atlassian.jira.jira-projects-plugin:sidebar-project-shortcuts/sidebar-project-shortcuts-Add.js?locale=en-PL:38:27\"",
                        "\"DEPRECATED JS - Inline dialog constructor has been deprecated and will be removed in a future release. Use inline dialog 2 instead. \" \" \\n initialize@http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/6.0.7/_/download/resources/com.atlassian.jira.jira-projects-plugin:sidebar-project-shortcuts/sidebar-project-shortcuts-Add.js?locale=en-PL:38:27\""
                )
        ));
    }


    @Test
    public void shouldParseWarnFromLogFileWithRegexFromSystemPropertyDefaultValues() throws URISyntaxException {
        // Set system property so that it will be used in the constructor
        System.setProperty("webdriver.firefox.devtools.console.regex", "");

        subject = new FirefoxLogParsingJavaScriptErrorRetriever(Paths.get(getFilePath()).toString(), getLogRegexPatternFromSystemProperty());

        final Iterable<JavaScriptErrorInfo> errors = subject.getErrors();

        Assert.assertThat(errors, contains(
                new JavaScriptErrorInfoImpl(
                        "http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/4.3.9-SNAPSHOT/_/download/resources/com.atlassian.jira.plugins.jira-wiki-editor:wiki-editor-resources/UndoManager.js?locale=en-PL, line 11: TypeError: test is undefined and other error: ble ble",
                        "http://localhost:2990/jira/s/7a119f5a2a0794581a49ad19e4654043-CDN/4iy93h/814000/6411e0087192541a09d88223fb51a6a0/4.3.9-SNAPSHOT/_/download/resources/com.atlassian.jira.plugins.jira-wiki-editor:wiki-editor-resources/UndoManager.js?locale=en-PL, line 11: TypeError: test is undefined and other error: ble ble"
                ),
                new JavaScriptErrorInfoImpl(
                        "some other console error: error e",
                        "some other console error: error e"
                )
        ));

    }

    @Test
    public void shouldReportThatErrorRetrievalIsSupported() {
        Assert.assertThat(new FirefoxLogParsingJavaScriptErrorRetriever("file.exists").isErrorRetrievalSupported(), equalTo(true));

        Assert.assertThat(new FirefoxLogParsingJavaScriptErrorRetriever(null).isErrorRetrievalSupported(), equalTo(false));
    }

    @Test
    public void getErrorsShouldDrainAndCauseSubsequentCallToReturnEmptyString() throws IOException, URISyntaxException {
        final Path tmpFile = Files.createTempFile("unit-test", ".tmp");
        Files.copy(Paths.get(getFilePath()), tmpFile, StandardCopyOption.REPLACE_EXISTING);
        subject = new FirefoxLogParsingJavaScriptErrorRetriever(tmpFile.toString());

        final Iterable<JavaScriptErrorInfo> errorsBeginning = subject.getErrors();

        Assert.assertThat(errorsBeginning, Matchers.iterableWithSize(2));

        final Iterable<JavaScriptErrorInfo> errorsAfterDrain = subject.getErrors();

        Assert.assertThat(errorsAfterDrain, Matchers.emptyIterable());

        appendString(tmpFile, "\nJavaScript error: other js error");

        final Iterable<JavaScriptErrorInfo> errorsAfterAppendingMoreLines = subject.getErrors();

        Assert.assertThat(errorsAfterAppendingMoreLines, contains(
                new JavaScriptErrorInfoImpl(
                        "other js error",
                        "other js error"
                )
        ));
    }

    private void appendString(Path tmpFile, String content) throws IOException {
        Files.write(
                tmpFile,
                content.getBytes(),
                StandardOpenOption.APPEND);
    }

    private URI getFilePath() throws URISyntaxException {
        return getClass().getClassLoader().getResource("firefox-example.log").toURI();
    }
}
