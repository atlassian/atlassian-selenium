package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;


public class LocalWebDriverFactoryTest {
    @Test
    public void getBrowserFromWholeValue() {
        assertThat(LocalWebDriverFactory.getBrowser("firefox"), equalTo(Browser.FIREFOX));
        assertThat(LocalWebDriverFactory.getBrowser("chrome"), equalTo(Browser.CHROME));
        assertThat(LocalWebDriverFactory.getBrowser("chrome_90"), equalTo(Browser.CHROME));
    }

    @Test
    public void getBrowserFromFirstRegexpGroup() {
        assertThat(LocalWebDriverFactory.getBrowser("firefox:path="), equalTo(Browser.FIREFOX));
        assertThat(LocalWebDriverFactory.getBrowser("chrome:path=abc"), equalTo(Browser.CHROME));
        assertThat(LocalWebDriverFactory.getBrowser("chrome_90:path=abc"), equalTo(Browser.CHROME));
    }

    @Test
    public void getBrowserPathWhenNoRegexpMatch() {
        assertThat(LocalWebDriverFactory.getBrowserPath("firefox:path"), nullValue());
        assertThat(LocalWebDriverFactory.getBrowserPath("firefox:abc"), nullValue());
        assertThat(LocalWebDriverFactory.getBrowserPath("firefox"), nullValue());
    }

    @Test
    public void getBrowserPathFromSecondRegexpGroup() {
        assertThat(LocalWebDriverFactory.getBrowserPath("firefox:path="), equalTo(""));
        assertThat(LocalWebDriverFactory.getBrowserPath("firefox:path=abc"), equalTo("abc"));
    }
}