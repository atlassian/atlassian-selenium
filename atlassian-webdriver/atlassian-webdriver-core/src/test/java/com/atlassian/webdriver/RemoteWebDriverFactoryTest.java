package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.openqa.selenium.Capabilities;

import java.net.URL;

import static com.atlassian.webdriver.WebDriverProperties.WEBDRIVER_BROWSER_HEADLESS;
import static com.atlassian.webdriver.browsers.firefox.FirefoxBrowser.FIREFOX_WEBDRIVER_LOGFILE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.endsWith;

public class RemoteWebDriverFactoryTest {

    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Test
    public void checkRegexpMatchesRemote() {
        assertThat(RemoteWebDriverFactory.matches("firefox"), is(false));
        assertThat(RemoteWebDriverFactory.matches("firefox:url="), is(true));
        assertThat(RemoteWebDriverFactory.matches("firefox:url=http://localhost/"), is(true));
    }

    @Test
    public void getBrowserFromFirstRegexpGroup() {
        assertThat(RemoteWebDriverFactory.getBrowser("firefox:url="), is(Browser.FIREFOX));
        assertThat(RemoteWebDriverFactory.getBrowser("chrome:url=http://localhost/"), is(Browser.CHROME));
    }

    @Test
    public void getBrowserDefaultsToFirefox() {
        // not a "browser:url=path" pattern, so returns firefox
        assertThat(RemoteWebDriverFactory.getBrowser("chrome"), is(Browser.FIREFOX));
        assertThat(RemoteWebDriverFactory.getBrowser(""), is(Browser.FIREFOX));
        assertThat(RemoteWebDriverFactory.getBrowser("chrome:path=abc"), is(Browser.FIREFOX));
    }

    @Test
    public void buildServerUrlAppendsWdHub() {
        // no need to append
        assertThat(RemoteWebDriverFactory.buildServerUrl("http://localhost:4444/wd/hub").toString(),
                is("http://localhost:4444/wd/hub"));
        assertThat(RemoteWebDriverFactory.buildServerUrl("http://localhost/wd/hub").toString(),
                is("http://localhost/wd/hub"));
        assertThat(RemoteWebDriverFactory.buildServerUrl("http://localhost/wd/hub/").toString(),
                is("http://localhost/wd/hub/"));

        // append and add slash if needed
        assertThat(RemoteWebDriverFactory.buildServerUrl("http://localhost:4444").toString(),
                is("http://localhost:4444/wd/hub"));
        assertThat(RemoteWebDriverFactory.buildServerUrl("http://localhost:4444/").toString(),
                is("http://localhost:4444/wd/hub"));
        assertThat(RemoteWebDriverFactory.buildServerUrl("http://localhost:4444/service").toString(),
                is("http://localhost:4444/service/wd/hub"));
    }

    @Test
    public void buildServerUrlFallsBackToDefault() {
        assertThat(RemoteWebDriverFactory.buildServerUrl("not:::/a!valid&url"),
                is(RemoteWebDriverFactory.defaultServerUrl()));
    }

    @Test
    public void defaultServerUrlIsNotNull() {
        final URL defaultUrl = RemoteWebDriverFactory.defaultServerUrl();
        assertThat(defaultUrl, notNullValue());
        assertThat(defaultUrl.toString(), is("http://localhost:4444/wd/hub"));
    }


    // check moz:firefoxOptions is set with args for headless mode
    @Test
    public void firefoxBrowserSpecificCapabilities() {
        WEBDRIVER_BROWSER_HEADLESS.setSystemProperty("true");

        final Capabilities capabilities = RemoteWebDriverFactory.getBrowserSpecificCapabilities(Browser.FIREFOX);
        String options = capabilities.asMap().get("moz:firefoxOptions").toString();
        assertThat(options, containsString("args"));
        assertThat(options, containsString("-headless"));
    }

    //  Check that the log file is set when the system property is set
    @Test
    public void firefoxBrowserLogFileSetFromSystemProperty() {
        System.setProperty(FIREFOX_WEBDRIVER_LOGFILE, "firefox.log");

        final Capabilities capabilities = RemoteWebDriverFactory.getBrowserSpecificCapabilities(Browser.FIREFOX);
        String options = capabilities.asMap().get("moz:firefoxOptions").toString();
        assertThat(options, containsString("args"));
        assertThat(System.getProperty(FIREFOX_WEBDRIVER_LOGFILE), endsWith("firefox.log"));
    }


    //  Check that the log file is set to a temporary file when the system property NOT set
    @Test
    public void firefoxBrowserLogFileSetWhenSystemPropertyIsNull() {
        System.setProperty(FIREFOX_WEBDRIVER_LOGFILE, "");

        final Capabilities capabilities = RemoteWebDriverFactory.getBrowserSpecificCapabilities(Browser.FIREFOX);
        String options = capabilities.asMap().get("moz:firefoxOptions").toString();
        assertThat(options, containsString("args"));
        assertThat(System.getProperty(FIREFOX_WEBDRIVER_LOGFILE), containsString("firefox-test-log"));
        assertThat(System.getProperty(FIREFOX_WEBDRIVER_LOGFILE), endsWith(".tmp"));
    }

    // check goog:chromeOptions is set with args for headless mode
    @Test
    public void chromeBrowserSpecificCapabilities() {
        WEBDRIVER_BROWSER_HEADLESS.setSystemProperty("true");

        final Capabilities capabilities = RemoteWebDriverFactory.getBrowserSpecificCapabilities(Browser.CHROME);
        String options = capabilities.asMap().get("goog:chromeOptions").toString();
        assertThat(options, containsString("args"));
        assertThat(options, containsString("-headless"));
    }

    // check unknown browser is handled
    @Test
    public void unknownBrowserSpecificCapabilities() {
        WEBDRIVER_BROWSER_HEADLESS.setSystemProperty("true");

        final Capabilities capabilities = RemoteWebDriverFactory.getBrowserSpecificCapabilities(Browser.UNKNOWN);
        assertThat(capabilities, notNullValue());
    }
}