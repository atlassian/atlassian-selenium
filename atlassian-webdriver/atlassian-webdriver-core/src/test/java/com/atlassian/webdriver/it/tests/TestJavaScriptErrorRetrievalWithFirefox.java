package com.atlassian.webdriver.it.tests;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.RequireBrowser;
import com.atlassian.webdriver.browsers.firefox.FirefoxBrowser;
import com.atlassian.webdriver.it.AbstractSimpleServerTest;
import com.atlassian.webdriver.it.pageobjects.SimpleTestedProduct;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.CspErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.IncludedScriptErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.NoErrorsPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.UntypedErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.WindowErrorPage;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import com.atlassian.webdriver.testing.simpleserver.SimpleServerRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule.ERROR_SEPARATOR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

/**
 * Test the rule for logging client-side console output.
 * <p>
 * At the time of writing, Firefox does not support this but this has changed as currently firefox is supported by parsing console logs redirected to stdout.
 * Ignoring errors seems not to be working as there is some parsing of error messages (possibly webdriver api format/chrome format)
 * see {@link TestJavaScriptErrorRetrievalWithChrome#testCanBeOverriddenToIgnoreSpecificErrors()}.
 */
@RequireBrowser(Browser.FIREFOX)
@TestedProductClass(SimpleTestedProduct.class)
@RunWith(SimpleServerRunner.class)
public class TestJavaScriptErrorRetrievalWithFirefox extends AbstractSimpleServerTest {

    @Inject
    private static JavaScriptErrorsRule rule;

    @Test
    public void testPageWithNoErrors() {
        product.visit(NoErrorsPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, equalTo(""));
    }

    @Test
    public void testSingleErrorInWindowScope() {
        final Page page = product.visit(WindowErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("ReferenceError: foo is not defined"));
        assertThat(consoleOutput, containsString(page.getUrl()));
    }

    @Test
    public void testErrorIncludesLineInfo() {
        final Page page = product.visit(WindowErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString(page.getUrl() + ", line 11"));
    }

    @Test
    public void testMultipleErrorsInIncludedScripts() {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("TypeError: undefined is not a function"));
        assertThat(consoleOutput, containsString(page.objectIsNotFunctionScriptUrl()));

        assertThat(consoleOutput, containsString("Error: throw Error('bail')"));
        assertThat(consoleOutput, containsString(page.throwErrorObjectScriptUrl()));
    }

    @Test
    public void testCspSupported() {
        final CspErrorPage page = product.visit(CspErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        // because of CSP support there's no errors present in the logs because scripts are not loaded
        assertThat(consoleOutput, equalTo(""));
    }

    @Test
    public void testEachJSErrorIsOnADoubleNewLine() {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        final String[] errors = consoleOutput.split(ERROR_SEPARATOR);

        assertThat(errors.length, equalTo(2));
        assertThat(errors[0], containsString(page.objectIsNotFunctionScriptUrl()));
        assertThat(errors[1], containsString(page.throwErrorObjectScriptUrl()));
    }

    @Test
    public void testCanCaptureUntypedErrors() {
        product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, anyOf(
                //different FF report either line 0 or line 1
                containsString("line 0: uncaught exception: throw string"),
                containsString("line 1: uncaught exception: throw string")
                )
        );
    }

    /**
     * turning on {@link FirefoxBrowser#FIREFOX_DEVTOOLS_CONSOLE_STDOUT_CONTENT} causes the console api to log to stdout.
     * Sadly it seems that log output from the "throw exception" and from "console.error" can be mixed together eg,
     * <pre>
     * JavaScript error: , line 0: uncaught exceptioncon:sole.error: "console.e rror"
     * throw string
     * </pre>
     * instead of
     * <pre>
     * JavaScript error: , line 0: uncaught exception: throw string
     * console.error: "console.error"
     * </pre>
     * <p>
     * If you add proper support for console.error please update this test.
     */
    @Test
    public void testCanNotCaptureConsoleErrors() {
        final UntypedErrorPage page = product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, not(containsString("console.error")));
    }
}
