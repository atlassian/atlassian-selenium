package com.atlassian.webdriver.debug;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WrapsDriver;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermissions;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

/**
 * Test case for {@link WebDriverDebug}.
 *
 * @since 2.2
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class TestWebDriverDebug
{

    private WebDriver mockDriver;
    private WebDriver mockWrappingDriver;
    private WebDriver mockDriverTakingScreenshot;
    private WebDriver mockDriverNotTakingScreenshot;
    private final Charset CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS = UTF_8;

    @Rule public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Before
    public void initDrivers()
    {
        mockDriver = mock(WebDriver.class);
        mockDriverTakingScreenshot = mock(WebDriver.class, withSettings().extraInterfaces(TakesScreenshot.class));
        mockDriverNotTakingScreenshot = mock(WebDriver.class);
        mockWrappingDriver = mock(WebDriver.class, withSettings().extraInterfaces(WrapsDriver.class));
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void shouldNotAcceptNullDriver()
    {
        new WebDriverDebug(null);

    }

    @Test
    public void shouldReturnCurrentUrl()
    {
        when(mockDriver.getCurrentUrl()).thenReturn("http://some-url.com");
        assertEquals("http://some-url.com", new WebDriverDebug(mockDriver).getCurrentUrl());

    }

    @Test
    public void dumpPageSourceToNonExistingFile() throws IOException
    {
        final File dumpDir = temporaryFolder.newFolder("TestWebDriverDebug");
        final File dumpFile = new File(dumpDir, "test.html");
        when(mockDriver.getPageSource()).thenReturn("<html>Awesome</html>");
        final WebDriverDebug debug = new WebDriverDebug(mockDriver);
        assertTrue("Should dump HTML source successfully", debug.dumpSourceTo(dumpFile));
        assertEquals("<html>Awesome</html>", readFileToString(dumpFile, CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS));
    }

    @Test
    public void dumpPageSourceToExistingFile() throws IOException
    {
        final File dumpFile = temporaryFolder.newFile("test.html");
        writeStringToFile(dumpFile, "blah blah", UTF_8);
        when(mockDriver.getPageSource()).thenReturn("<html>Awesome no blah</html>");
        final WebDriverDebug debug = new WebDriverDebug(mockDriver);
        assertTrue("Should dump HTML source successfully", debug.dumpSourceTo(dumpFile));
        // should overwrite the previous contents
        assertEquals("<html>Awesome no blah</html>", readFileToString(dumpFile, CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS));
    }

    @Test
    public void dumpPageShouldNotThrowExceptionOnIoError() throws IOException
    {
        final File dumpFile = temporaryFolder.newFile("test.html");
        assertTrue(dumpFile.setWritable(false));
        when(mockDriver.getPageSource()).thenReturn("<html>Awesome</html>");
        final WebDriverDebug debug = new WebDriverDebug(mockDriver);
        assertFalse("Should fail to dump HTML source into non-writable file", debug.dumpSourceTo(dumpFile));
    }

    @Test
    public void dumpPageSourceToWriterShouldWorkGivenWorkingWriter()
    {
        when(mockDriver.getPageSource()).thenReturn("<html>Awesome</html>");
        final WebDriverDebug debug = new WebDriverDebug(mockDriver);
        final StringWriter output = new StringWriter();
        assertTrue("Should dump HTML source successfully", debug.dumpPageSourceTo(output));
        assertEquals("<html>Awesome</html>", output.toString());
    }

    @Test
    public void shouldTakeScreenshotGivenDriverTakesScreenshot() throws IOException
    {
        final File dumpDir = temporaryFolder.newFolder("TestWebDriverDebug");
        final File fakeScreenshot = temporaryFolder.newFile("fake.png");
        final File testOutput = new File(dumpDir, "test.png");
        writeStringToFile(fakeScreenshot, "FAKE SCREENSHOT!", CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS);
        when(mockDriverTakingScreenshot().getScreenshotAs(OutputType.FILE)).thenReturn(fakeScreenshot);
        final WebDriverDebug debug = new WebDriverDebug(mockDriverTakingScreenshot);
        assertTrue(debug.takeScreenshotTo(testOutput));
        assertEquals("FAKE SCREENSHOT!", readFileToString(testOutput, CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS));
    }

    @Test
    public void shouldTakeScreenshotGivenUnderlyingDriverTakesScreenshot() throws IOException
    {
        final File dumpDir = temporaryFolder.newFolder("TestWebDriverDebug");
        final File fakeScreenshot = temporaryFolder.newFile("fake.png");
        final File testOutput = new File(dumpDir, "test.png");
        writeStringToFile(fakeScreenshot, "FAKE SCREENSHOT!", CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS);
        when(mockDriverTakingScreenshot().getScreenshotAs(OutputType.FILE)).thenReturn(fakeScreenshot);
        stubWrappedDriver(mockDriverTakingScreenshot);
        final WebDriverDebug debug = new WebDriverDebug(mockWrappingDriver);
        assertTrue(debug.takeScreenshotTo(testOutput));
        assertEquals("FAKE SCREENSHOT!", readFileToString(testOutput, CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS));
    }

    @Test
    public void shouldNotTakeScreenshotGivenNoUnderlyingDriverTakesScreenshot() throws IOException
    {
        final File dumpDir = temporaryFolder.newFolder("TestWebDriverDebug");
        final File fakeScreenshot = temporaryFolder.newFile("fake.png");
        final File testOutput = new File(dumpDir, "test.png");
        writeStringToFile(fakeScreenshot, "FAKE SCREENSHOT!", CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS);
        when(mockDriverTakingScreenshot().getScreenshotAs(OutputType.FILE)).thenReturn(fakeScreenshot);
        stubWrappedDriver(mockDriverNotTakingScreenshot);
        final WebDriverDebug debug = new WebDriverDebug(mockWrappingDriver);
        assertFalse("Screenshot should not be taken", debug.takeScreenshotTo(testOutput));
    }

    @Test
    public void shouldNotTakeScreenshotGivenIoError() throws IOException
    {
        final File fakeScreenshot = temporaryFolder.newFile("fake.png");

        Path noWriteDir = temporaryFolder.newFolder("noWriteDir").toPath();
        Path readOnlyFile = noWriteDir.resolve("dest.png");
        Files.createFile(readOnlyFile);
        // Set noWriteDir directory perms to r-xr-xr-x (read & execute only, no write)
        // as setting permission to only file or setting it to not writable does nothing, and you can still copy
        Files.setPosixFilePermissions(noWriteDir, PosixFilePermissions.fromString("r-xr-xr-x"));

        writeStringToFile(fakeScreenshot, "FAKE SCREENSHOT!", CHARSET_FOR_FILES_WRITTEN_BY_TEST_HELPERS);
        when(mockDriverTakingScreenshot().getScreenshotAs(OutputType.FILE)).thenReturn(fakeScreenshot);
        stubWrappedDriver(mockDriverTakingScreenshot);
        final WebDriverDebug debug = new WebDriverDebug(mockWrappingDriver);
        assertFalse("Screenshot should not be taken given output file not writable", debug.takeScreenshotTo(readOnlyFile.toFile()));
    }


    private void stubWrappedDriver(WebDriver wrappedDriver)
    {
        final WrapsDriver wrapsDriver = (WrapsDriver) mockWrappingDriver;
        when(wrapsDriver.getWrappedDriver()).thenReturn(wrappedDriver);
    }

    private TakesScreenshot mockDriverTakingScreenshot()
    {
        return (TakesScreenshot) mockDriverTakingScreenshot;
    }
}
