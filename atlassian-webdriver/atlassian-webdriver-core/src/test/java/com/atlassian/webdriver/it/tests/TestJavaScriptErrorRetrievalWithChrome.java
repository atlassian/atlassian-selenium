package com.atlassian.webdriver.it.tests;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.RequireBrowser;
import com.atlassian.webdriver.it.AbstractSimpleServerTest;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.CspErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.IncludedScriptErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.NoErrorsPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.UntypedErrorPage;
import com.atlassian.webdriver.it.pageobjects.page.jsconsolelogging.WindowErrorPage;
import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;

import java.util.regex.Pattern;

import static com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule.ERROR_SEPARATOR;
import static java.lang.String.format;
import static java.util.regex.Pattern.compile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.stringContainsInOrder;

/**
 * Test the rule for logging client-side console output.
 *
 * At the time of writing, the logging only captures exceptions.
 * The tests only work in Chrome, since Chrome is the only browser to implement retrieval of logs remotely.
 */
@RequireBrowser(Browser.CHROME)
public class TestJavaScriptErrorRetrievalWithChrome extends AbstractSimpleServerTest
{

    private JavaScriptErrorsRule rule;

    @Before
    public void setUp()
    {
        rule = new JavaScriptErrorsRule(product.getTester().getDriver());
    }

    @Test
    public void testPageWithNoErrors()
    {
        product.visit(NoErrorsPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, equalTo(""));
    }

    @Test
    public void testSingleErrorInWindowScope()
    {
        final Page page = product.visit(WindowErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("ReferenceError: foo is not defined"));
        assertThat(consoleOutput, containsString(page.getUrl()));
    }

    @Test
    public void testErrorIncludesLineAndColumnInfo() {
        final Page page = product.visit(WindowErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString(page.getUrl() + " 10:21"));
    }

    @Test
    public void testMultipleErrorsInIncludedScripts()
    {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("TypeError: undefined is not a function"));
        assertThat(consoleOutput, containsString(page.objectIsNotFunctionScriptUrl()));

        assertThat(consoleOutput, containsString("Error: throw Error('bail')"));
        assertThat(consoleOutput, containsString(page.throwErrorObjectScriptUrl()));
    }

    @Test
    public void testCspError()
    {
        final CspErrorPage page = product.visit(CspErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();

        final Pattern expectedError1Pattern = compile(format("Refused to load the script '.*%s' because it violates the following Content Security Policy directive:.*", page.objectIsNotFunctionScriptUrl()));
        assertThat(format("Console output should match '%s': '%s'", expectedError1Pattern, consoleOutput), expectedError1Pattern.matcher(consoleOutput).find(), is(true));
        final Pattern expectedError2Pattern = compile(format("Refused to load the script '.*%s' because it violates the following Content Security Policy directive:.*", page.throwErrorObjectScriptUrl()));
        assertThat(format("Console output should match '%s': '%s'", expectedError2Pattern, consoleOutput), expectedError2Pattern.matcher(consoleOutput).find(), is(true));
        assertThat(consoleOutput, not(containsString("TypeError: undefined is not a function")));
        assertThat(consoleOutput, not(containsString("Error: throw Error('bail')")));
    }

    @Test
    public void testEachJSErrorIsOnADoubleNewLine()
    {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        final String[] errors = consoleOutput.split(ERROR_SEPARATOR);

        assertThat(errors.length, equalTo(2));
        assertThat(errors[0], containsString(page.objectIsNotFunctionScriptUrl()));
        assertThat(errors[1], containsString(page.throwErrorObjectScriptUrl()));
    }

    @Test
    public void testErrorsIncludeCategories() {
        final IncludedScriptErrorPage page = product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        final String[] errors = consoleOutput.split(ERROR_SEPARATOR);

        assertThat(errors[0], stringContainsInOrder(ImmutableList.of("SEVERE","TypeError: undefined is not a function")));
        assertThat(errors[1], stringContainsInOrder(ImmutableList.of("SEVERE","Error: throw Error('bail')")));
    }

    @Test
    public void testCanCaptureUntypedErrors()
    {
        product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("0:0 Uncaught"));
    }

    @Test
    public void testCanBeOverriddenToIgnoreSpecificErrors()
    {
        rule = rule.errorsToIgnore(ImmutableSet.of("Uncaught TypeError: undefined is not a function"));
        product.visit(IncludedScriptErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, not(containsString("Uncaught TypeError: undefined is not a function")));
    }

    @Test
    public void testCanCaptureConsoleErrors()
    {
        final UntypedErrorPage page = product.visit(UntypedErrorPage.class);
        final String consoleOutput = rule.getConsoleOutput();
        assertThat(consoleOutput, containsString("console.error"));
        assertThat(consoleOutput, containsString(page.consoleErrorScriptUrl()));
    }
}
