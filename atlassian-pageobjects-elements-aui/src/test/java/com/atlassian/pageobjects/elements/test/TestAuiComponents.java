package com.atlassian.pageobjects.elements.test;

import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.IgnoreBrowser;
import com.atlassian.pageobjects.elements.test.pageobjects.page.AuiPage;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.junit.Test;

import jakarta.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Conditions.forSupplier;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestAuiComponents extends AbstractPageElementBrowserTest
{
    @Inject
    private Timeouts timeouts;

    @Test
    public void testAuiMenu()
    {
        AuiPage auipage = product.visit(AuiPage.class);

        //verify items in menu
        List<String> items = auipage.openLinksMenu().getItems();
        assertEquals(6, items.size());
        assertEquals("AUI Page", items.get(0));
        for(int i = 1; i <6; i++)
        {
            assertEquals("Item " + i, items.get(i));
        }

        // click on an item in the menu
        auipage.openLinksMenu().gotoAuiPage();
    }

    @Test
    public void testsAuiTab()
    {
        AuiPage auipage = product.visit(AuiPage.class);

        // verify default selections
        assertTrue(auipage.roleTabs().adminTab().isOpen());
        assertFalse(auipage.roleTabs().userTab().isOpen());

        // open user tab and verify header and selections
        assertEquals("This is User Tab", auipage.roleTabs().openUserTab().header());
        assertFalse(auipage.roleTabs().adminTab().isOpen());
        assertTrue(auipage.roleTabs().userTab().isOpen());

        // Open admin and verify header and selections
        assertEquals("This is Admin Tab", auipage.roleTabs().openAdminTab().header());
        assertTrue(auipage.roleTabs().adminTab().isOpen());
        assertFalse(auipage.roleTabs().userTab().isOpen());
    }

    @Test
    public void testsInlineDialog()
    {
        AuiPage auipage = product.visit(AuiPage.class);

        // verify dialog is not.
        assertFalse(auipage.inlineDialog().getView().isPresent());

        // Invoke dialog and verify contents and is visible
        assertEquals("AUI Inline Dialog", auipage.openInlineDialog().content());
        assertTrue(auipage.inlineDialog().getView().isVisible());

        // click somewhere else and verify dialog is not visible.
        waitUntilFalse(forSupplier(timeouts, () -> {
            auipage.roleTabs().openUserTab();
            return auipage.inlineDialog().getView().isVisible();
        }));
    }
}
