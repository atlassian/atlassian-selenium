# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [4.0.0]

### Modified
- We've switched from Selenium 3.x to 4.6.0:
  - Mobile devices are no longer a separate browsers specified in `Browser`, please use normal browser and use the platform capability of `ios`/`android`,
  - Selenium 4 does not use Apache HTTP Client anymore, we're now using Netty to do the heavy-lifting.
  - Opera is using Chrome driver now, please set binary path when running that browser.

## [Unreleased]
## [3.8.0]

## [3.7.0]

### Added

- Added support for jakarta.inject, while still supporting javax.inject
- bumped Guice to 6.0
- Added an option to capture partial snapshots of the page using
  `com.atlassian.webdriver.testing.rule.VisualRegressionRule.captureId(java.lang.String, org.openqa.selenium.WebElement)`.
  This is a helpful utility for visual regression tests as it allows you to isolate a
  specific subsection of the page through specification of a parent element's id

## [3.6.0]

### Added

- Added an option to set Firefox preferences using `webdriver.firefox.remote.preferences` system property, for remote
  webdriver instances

## [3.5.1]

### Added

- Added Poller#waitUntil to support the assertion by using a `java.util.function.Predicate` to wait until the condition
  is met
  The current method using `org.hamrcrest.Matchers` is still there and very likely to be deprecated in the future
- `Poller#waitUntilTruePredicate` as parity for utility of method `Poller#waitUntilTrue`
- `Poller#waitUntilFalsePredicate` as parity for utility of method `Poller#waitUntilFalse`
- `Poller#waitUntilEqualsPredicate` as parity for utility of method `Poller#waitUntilEquals`

## [3.5.0]

### Added

- Added an option to set Firefox profile preferences with `webdriver.firefox.profile.preferences` system property

## [3.4.0]

### Modified

- `Capabilities` created using `WebDriverFactory` are now merged with default capabilities for the requested browser,
  similarly to `RemoteWebDriverFactory`

### Dependencies

- Updated com.browserstack:browserstack-local-java from 1.0.9 to v1.1.4
- Updated org.apache.commons:commons-lang3 from 3.12.0 to 3.14.0
- Updated net.bytebuddy:byte-buddy from 1.12.23 to 1.14.18

## [3.3.7]

### Added

- `Poller#waitUntilTruePredicate` as parity for utility of method `Poller#waitUntilTrue`
- `Poller#waitUntilFalsePredicate` as parity for utility of method `Poller#waitUntilFalse`
- `Poller#waitUntilEqualsPredicate` as parity for utility of method `Poller#waitUntilEquals`

## [3.3.6]

### Added

- Added `Poller#waitUntil` to support the assertion by using a `java.util.function.Predicate` to wait until the
  condition is met
  The current method using `org.hamrcrest.Matchers` is still there and very likely to be deprecated in the future

## [3.3.5]

### Added

- Added "--headless" and "--disable-dev-shm-usage" to allow to run Chrome Docker
  3.141.59-20210929 aka Chrome 94.0.4606.61

## [3.3.4]

### Added

- Added "webdriver.firefox.devtools.console.stdout" JVM properties, which
  allows specifying writing devtools console to stdout for Firefox browser
- Added "webdriver.firefox.devtools.console.regex" JVM properties, which
  allows specifying regex `FirefoxLogParsingJavaScriptErrorRetriever` when filtering and extracting console logs
- Fix to set default capabilities for Firefox and Chrome when using `RemoteWebDriver`

### Dependencies

- [BSP-5926](https://bulldog.internal.atlassian.com/browse/BSP-5926) Added 'org.json' to dependencies due to absence of
  the release of browserstack-local-java.

## [3.2.10]

Changes brought forward from [3.1.11]

### Added

- Added "webdriver.logging.driver", "webdriver.logging.browser" and "webdriver.logging.client" JVM properties, which
  allows specifying logging levels for Selenium components
- The "webdriver.capabilities" property can be passed to a local web driver

### Modified

- `FirefoxBrowser`, `ChromeBrowser` and `IeBrowser` accept the `Capabilities` object as an additional argument
- `FirefoxBrowser` and `ChromeBrowser` inherit from a common `AbstractBrowser`
- The `Browser.typeOf` uses case-insensitive matching (atlassian-pageobjects-api and atlassian-selenium)

### Removed

- The SEVERE logging level for `LogType.BROWSER` is no longer set by default. You can pass the
  `-Dwebdriver.logging.browser=SEVERE` JVM property instead.

## [3.2.9]

Changes brought forward from [3.1.10]

### Added

- The `BrowserstackWebDriverFactory` can read browserstack.resolution, browserstack.console, browserstack.debug,
  browserstack.networkLogs and browserstack.video system properties and pass in the `DesiredCapabilities` for
  the driver it creates
- The `BrowserstackWebDriverFactory` can read webdriver.capabilities system property, containing a list of key=value
  pairs, and pass them in the `DesiredCapabilities` for the driver it creates

## [3.2.8]

### Dependencies

- Upgraded atlassian-browsers to 3.1.4 (up from 3.1.2) for updating
  commons-io [BSP-2957](https://bulldog.internal.atlassian.com/browse/BSP-2957)

## [3.2.7]

### Changed

- Added new methods to WebDriverScreenshotRule to allow screenshots and page source to be captured at any time during a
  test

## [3.2.6]

Changes brought forward from [3.0.2]

### Dependencies

- Upgraded atlassian-browsers to 3.1.2 (up from 3.1.1)
- Upgraded Jetty to 9.4.39.v20210325 (up from 9.4.19.v20190610)

## [3.2.5]

Changes brought forward from [3.0.1]

### Dependencies

- Upgraded atlassian-browsers to 3.0.4 (up from 3.0.2)
- Upgraded JUnit to 4.13.2 (up from 4.12)
- Upgraded Jetty to 9.4.37.v20210219 (up from 9.4.19.v20190610)
- Upgraded Velocity to 1.6.4-atlassian-22 (up from 1.6.2)
- Upgraded Selenium Firefox driver to 3.141.59-atlassian-1 (up from 3.141.59)

## [3.1.11]

Changes brought forward from [3.0.5]

### Added

- Added "webdriver.logging.driver", "webdriver.logging.browser" and "webdriver.logging.client" JVM properties, which
  allows specifying logging levels for Selenium components
- The "webdriver.capabilities" property can be passed to a local web driver

### Modified

- `FirefoxBrowser`, `ChromeBrowser` and `IeBrowser` accept the `Capabilities` object as an additional argument
- `FirefoxBrowser` and `ChromeBrowser` inherit from a common `AbstractBrowser`
- The `Browser.typeOf` uses case-insensitive matching (atlassian-pageobjects-api and atlassian-selenium)

### Removed

- The SEVERE logging level for `LogType.BROWSER` is no longer set by default. You can pass the
  `-Dwebdriver.logging.browser=SEVERE` JVM property instead.

## [3.1.10]

Changes brought forward from [3.0.4]

### Added

- The `BrowserstackWebDriverFactory` can read browserstack.resolution, browserstack.console, browserstack.debug,
  browserstack.networkLogs and browserstack.video system properties and pass in the `DesiredCapabilities` for
  the driver it creates
- The `BrowserstackWebDriverFactory` can read webdriver.capabilities system property, containing a list of key=value
  pairs, and pass them in the `DesiredCapabilities` for the driver it creates

## [3.1.9]

### Dependencies

- Upgraded atlassian-browsers to 3.0.6 (up from 3.0.5) for updating
  commons-io [BSP-2955](https://bulldog.internal.atlassian.com/browse/BSP-2955) and
  commons-compress [BSP-2947](https://bulldog.internal.atlassian.com/browse/BSP-2947)

## [3.1.8]

Changes brought forward from [3.0.2]

### Dependencies

- Upgraded atlassian-browsers to 3.0.5 (up from 3.0.4)
- Upgraded Jetty to 9.4.39.v20210325 (up from 9.4.19.v20190610)

## [3.1.7]

Changes brought forward from [3.0.1]

### Dependencies

- Upgraded atlassian-browsers to 3.0.4 (up from 3.0.2)
- Upgraded JUnit to 4.13.2 (up from 4.12)
- Upgraded Jetty to 9.4.37.v20210219 (up from 9.4.19.v20190610)
- Upgraded Velocity to 1.6.4-atlassian-22 (up from 1.6.2)
- Upgraded Selenium Firefox driver to 3.141.59-atlassian-1 (up from 3.141.59)

## [3.1.1]

### Dependencies

- Upgraded Atlassian Browsers version to 3.1.1 (up from 3.1.0)
- Upgraded version contains new firefox profile with additional initial preferences (eg.
  dom.disable_onbeforeunload=false).

## [3.1.0]

### Dependencies

- Upgraded Atlassian Browsers version to 3.1.0 (up from 3.0.2)

## [3.0.5]

Changes brought forward from [2.6.4]

### Added

- Added "webdriver.logging.driver", "webdriver.logging.browser" and "webdriver.logging.client" JVM properties, which
  allows specifying logging levels for Selenium components
- The "webdriver.capabilities" property can be passed to a local web driver

### Modified

- `FirefoxBrowser`, `ChromeBrowser` and `IeBrowser` accept the `Capabilities` object as an additional argument
- `FirefoxBrowser` and `ChromeBrowser` inherit from a common `AbstractBrowser`
- The `Browser.typeOf` uses case-insensitive matching (atlassian-pageobjects-api and atlassian-selenium)

### Removed

- The SEVERE logging level for `LogType.BROWSER` is no longer set by default. You can pass the
  `-Dwebdriver.logging.browser=SEVERE` JVM property instead.

## [3.0.4]

Changes brought forward from [2.6.3]

### Added

- The `BrowserstackWebDriverFactory` can read browserstack.resolution, browserstack.console, browserstack.debug,
  browserstack.networkLogs and browserstack.video system properties and pass in the `DesiredCapabilities` for
  the driver it creates
- The `BrowserstackWebDriverFactory` can read webdriver.capabilities system property, containing a list of key=value
  pairs, and pass them in the `DesiredCapabilities` for the driver it creates

## [3.0.3]

### Dependencies

- Upgraded atlassian-browsers to 3.0.6 (up from 3.0.5) for updating
  commons-io [BSP-2955](https://bulldog.internal.atlassian.com/browse/BSP-2955) and
  commons-compress [BSP-2947](https://bulldog.internal.atlassian.com/browse/BSP-2947)

## [3.0.2]

### Dependencies

- Upgraded atlassian-browsers to 3.0.5 (up from 3.0.4)
- Upgraded Jetty to 9.4.39.v20210325 (up from 9.4.19.v20190610)

## [3.0.1]

### Added

- `RemoteWebDriverFactory` will allow uploading local files to remote browser.
- `browserstack.idleTimeout` config option can be passed to Browserstack browser; it has a default value of 90 (
  seconds).

### Changed

- [Patched XXE in Selenium Firefox driver by forking](https://github.com/atlassian-forks/selenium/commit/448eda9007ec2c18e3a65714a643d5ec836109fc)
- Can set Browserstack authentication token value using any of the following env variables: `BROWSERSTACK_ACCESS_KEY`,
  `BROWSERSTACK_ACCESSKEY`, `BROWSERSTACK_KEY`, `BROWSERSTACK_AUTH`

### Dependencies

- Upgraded atlassian-browsers to 3.0.4 (up from 3.0.2)
- Upgraded JUnit to 4.13.2 (up from 4.12)
- Upgraded Jetty to 9.4.37.v20210219 (up from 9.4.19.v20190610)
- Upgraded Velocity to 1.6.4-atlassian-22 (up from 1.6.2)
- Upgraded Selenium Firefox driver to 3.141.59-atlassian-1 (up from 3.141.59)

## [3.0.0]

### Highlights

- Atlassian-selenium now uses webdriver v3!
- All deprecated code has been removed!

### Dependencies

- The minimum supported version of Java is 8.
- The minimum supported platform version is 5.
- Upgraded webdriver version to 3.141.59 (up from 2.51.1).
- Upgraded atlassian-browsers version to 3.0.2 (up from 2.8.8).
- Upgraded guava to version 26.0-jre (up from 18.0).
- Upgraded guice version to 4.2.2 (up from 3.0).
- Upgraded Apache httpclient version to 4.5.9 (up from 4.3.2).
- Upgraded Apache httpcore version to 4.4.11 (up from 4.3.2).
- Replaced Apache commons-lang with commons-lang3 version 3.9.

### Changed

- `com.atlassian.webdriver.AtlassianWebDriverModule` has been renamed to `com.atlassian.webdriver.WebDriverModule`.

### Removed

- Support for HtmlUnit has been removed.
- `AtlassianWebDriver` has been replaced by direct usage of the classes it wrapped:
- `com.atlassian.webdriver.WebDriverContext` from atlassian-webdriver-core, or
- `org.openqa.selenium.WebDriver` from selenium.

#### Projects

- atlassian-pageobjects-api: `BrowserUtil` has been removed. Use `BrowserAware` instead.
- atlassian-pageobjects-elements: All AUI-specific page objects have been extracted in to
  atlassian-pageobjects-elements-aui.
- atlassian-performance: This project has been deleted.
- atlassian-selenium: This project (which contained support for selenium v1) has been deleted. Use atlassian-webdriver
  instead.
- atlassian-selenium-browsers: This project (which contained selenium v1 browser bindings) has been deleted. Use the
  `LifecycleAwareWebDriverGrid` from atlassian-webdriver-core instead.
- atlassian-webdriver-confluence: This project has been deleted.
- atlassian-webdriver-greenhopper: This project has been deleted.
- atlassian-webdriver-jira: This project has been deleted.

## [2.6.4]

### Added

- Added "webdriver.logging.driver", "webdriver.logging.browser" and "webdriver.logging.client" JVM properties, which
  allows specifying logging levels for Selenium components
- The "webdriver.capabilities" property can be passed to a local web driver

### Modified

- `FirefoxBrowser`, `ChromeBrowser` and `IeBrowser` accept the `Capabilities` object as an additional argument
- `FirefoxBrowser` and `ChromeBrowser` inherit from a common `AbstractBrowser`
- The `Browser.typeOf` uses case-insensitive matching (atlassian-pageobjects-api and atlassian-selenium)

## [2.6.3]

### Added

- The `BrowserstackWebDriverFactory` can read browserstack.resolution, browserstack.console, browserstack.debug,
  browserstack.networkLogs and browserstack.video system properties and pass in the `DesiredCapabilities` for
  the driver it creates
- The `BrowserstackWebDriverFactory` can read webdriver.capabilities system property, containing a list of key=value
  pairs, and pass them in the `DesiredCapabilities` for the driver it creates

## [2.6.2]

### Added

- `RemoteWebDriverFactory` will allow uploading local files to remote browser.
- `browserstack.idleTimeout` config option can be passed to Browserstack browser; it has a default value of 90 (
  seconds).

### Changed

- Can set Browserstack authentication token value using any of the following env variables: `BROWSERSTACK_ACCESS_KEY`,
  `BROWSERSTACK_ACCESSKEY`, `BROWSERSTACK_KEY`, `BROWSERSTACK_AUTH`

## [2.6.1]

- Empty release.

## [2.6.0]

### Added

- Added support for connecting to
  Browserstack! [See the "Integrating with Browserstack" docs for details.](https://ecosystem.atlassian.net/wiki/spaces/SELENIUM/pages/692617339/Integrating+with+Browserstack)

## [2.5.0]

### Dependencies

- Upgraded selenium version to 2.53.1 (up from 2.51.0)

## [2.4.1]

### Fixed

- Fixed `JavaScriptErrorsRule` rendering none of the captured errors in logs.

## [2.4.0]

### Dependencies

- Upgraded selenium version to 2.51.0 (up from 2.46.0)
- Upgraded atlassian-browsers version to 2.8.1 (up from 2.7.1)
- Upgraded JSErrorCollector version to 0.6-atlassian-3 (up from 0.6-atlassian-1)
- HtmlUnit firefox browser version is 38 (up from 24)

### Fixed

- Fixed infinite recursive call in `GadgetView#getScreenshotAs` ([SELENIUM-280])

[3.6.0]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.6.0..atlassian-selenium-parent-3.5.1

[3.5.1]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.5.1..atlassian-selenium-parent-3.5.0

[3.2.8]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.2.8..atlassian-selenium-parent-3.2.7

[3.2.7]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.2.7..atlassian-selenium-parent-3.2.6

[3.2.6]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.2.6..atlassian-selenium-parent-3.2.5

[3.2.5]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.2.5..atlassian-selenium-parent-3.2.4

[3.2.0]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.2.0%0D3.1.x

[3.1.11]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.1.11..atlassian-selenium-parent-3.1.10

[3.1.10]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.1.10..atlassian-selenium-parent-3.1.9

[3.1.9]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.1.9..atlassian-selenium-parent-3.1.8

[3.1.8]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.1.8..atlassian-selenium-parent-3.1.7

[3.1.7]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.1.7..atlassian-selenium-parent-3.1.6

[3.1.1]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.1.1..atlassian-selenium-parent-3.1.0

[3.1.0]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.1.0%0D3.0.x

[3.0.5]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.0.5..atlassian-selenium-parent-3.0.4

[3.0.4]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.0.4..atlassian-selenium-parent-3.0.3

[3.0.3]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.0.3..atlassian-selenium-parent-3.0.2

[3.0.2]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.0.2..atlassian-selenium-parent-3.0.1

[3.0.1]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.0.1..atlassian-selenium-parent-3.0.0

[3.0.0]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-3.0.0..atlassian-selenium-parent-2.6.2

[2.6.4]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-2.6.4..atlassian-selenium-parent-2.6.3

[2.6.3]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-2.6.3..atlassian-selenium-parent-2.6.2

[2.6.2]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-2.6.2..atlassian-selenium-parent-2.6.0

[2.6.0]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-2.6.0..atlassian-selenium-parent-2.5.6

[2.5.0]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-2.5.0..atlassian-selenium-parent-2.4.6

[2.4.0]: https://bitbucket.org/atlassian/atlassian-selenium/compare/atlassian-selenium-parent-2.4.0..atlassian-selenium-parent-2.3.0
