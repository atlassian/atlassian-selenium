package com.atlassian.pageobjects.browser;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class BrowserTest {
    @Test
    public void ensureTypeOfIsCaseInsensitive() {
        assertThat(Browser.typeOf("FIREFOX"), is(Browser.FIREFOX));
        assertThat(Browser.typeOf("firefox"), is(Browser.FIREFOX));
    }

    @Test
    public void ensureTypeOfMatchesByPrefix() {
        assertThat(Browser.typeOf("Firefox"), is(Browser.FIREFOX));
        assertThat(Browser.typeOf("Firefox 90"), is(Browser.FIREFOX));
        assertThat(Browser.typeOf("Chrome 90.1"), is(Browser.CHROME));
    }

    @Test
    public void ensureTypeOfReturnsUnknownBrowser() {
        assertThat(Browser.typeOf("Non Existing Browser"), is(Browser.UNKNOWN));
    }
}