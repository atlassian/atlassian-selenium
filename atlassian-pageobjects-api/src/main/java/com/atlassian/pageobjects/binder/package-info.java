/**
 * The default binder implementation, {@link com.atlassian.pageobjects.binder.InjectPageBinder}, supporting jakarta.inject.Inject annotations and lifecycle methods.
 */
package com.atlassian.pageobjects.binder;