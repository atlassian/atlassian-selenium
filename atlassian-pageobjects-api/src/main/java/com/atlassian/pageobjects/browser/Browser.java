package com.atlassian.pageobjects.browser;

import java.util.Locale;

public enum Browser {
    ALL,
    FIREFOX,
    UNKNOWN,
    EDGE,
    CHROME;

    /**
     * @return a lowercase version of the name
     */
    public String getName() {
        return name().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Matches browser type based on its name prefix.
     * @return Browser - the browser matched or {@link #UNKNOWN}
     */
    public static Browser typeOf(String browserStartString) {
        final String browserLowerCase = browserStartString.toLowerCase(Locale.ENGLISH);
        for (Browser browser : Browser.values()) {
            if (browserLowerCase.startsWith(browser.getName())) {
                return browser;
            }
        }
        return UNKNOWN;
    }

}
