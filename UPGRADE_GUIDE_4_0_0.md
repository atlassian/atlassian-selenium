
# Upgrade Guide: Atlassian Selenium 3.x to 4.x

This guide outlines the steps and changes you need to be aware of when upgrading from Atlassian Selenium 3.x to Atlassian Selenium 4.x, Which uses the Selenium 4.

Selenium 4 introduces several API changes, new features, and deprecated or removed features. Follow this guide to ensure a smooth transition.

## 1. General Upgrade Steps

1. **Backup Your Test Suite**: Before starting the upgrade, ensure you have a backup of your current test suite.

2. **Test Your Suite**: Run your test suite to identify any breaking changes and areas that require modification.
## 2. API Changes
Selenium 4 introduces several API changes, enhancements, and new features:
### WebDriver Changes
- **W3C WebDriver Protocol**: Selenium 4 uses the W3C WebDriver standard by default, which may affect how certain capabilities are handled. Review your desired capabilities and update them if necessary.
- **New Window and Tab Handling**:
    - `driver.switchTo().newWindow(WindowType.TAB)` and `driver.switchTo().newWindow(WindowType.WINDOW)` are introduced to open new tabs and windows, respectively.
- **Relative Locators**: New relative locators, such as `above()`, `below()`, `toLeftOf()`, `toRightOf()`, and `near()`, allow for more intuitive element location.
### Element Location Strategies
- **Deprecated Methods**: `findElementBy*` methods have been deprecated. Use `By` locators instead, such as `By.id`, `By.name`, `By.className`, etc. For example, replace:

```
driver.findElementById("elementId");
```
with:
```
driver.findElement(By.id("elementId"));
```
### WebDriverWait Changes
FluentWait: Selenium 4 makes use of the Duration class from java.time package for specifying wait times. You need to update your WebDriverWait instantiations:

```
WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
```
Instead of the old way:

```
WebDriverWait wait = new WebDriverWait(driver, 10);
```

### RemoteWebDriver Changes
Session Handling: The SessionId and `getSessionId()` method are now part of the standard API, ensuring better handling of remote sessions.

## 3. Deprecated and Removed Features
- **DesiredCapabilities**: While DesiredCapabilities is still available, it is recommended to switch to Options classes (e.g., `ChromeOptions`, `FirefoxOptions`) which provide more specific configurations for each browser.
- **Legacy Browser Support**: Support for older browsers or versions not compliant with the W3C WebDriver specification might be limited or removed. Ensure you are using updated browser versions.
- **Native Events**: Native events are deprecated. This may affect how interactions are performed on certain platforms, particularly for older browser versions or environments.
- **Removed BrowserStack**: We do not support BrowserStack anymore.
- **Removed Browsers Support**: We removed IE, Opera and Safari from our API. 
## 4. New Features and Enhancements
- **DevTools Protocol**: Selenium 4 introduces support for Chrome DevTools Protocol, allowing deeper interaction with browsers for tasks such as network interception, performance monitoring, and more.
- **Improved Network Interceptions**: Enhanced capabilities for intercepting and modifying network traffic during tests, useful for mocking responses or simulating network conditions.
- **Enhanced Grid**: Selenium Grid has been rewritten with a new UI, better observability, and improved support for Docker. Configuration and setup have been simplified.
## 5. Testing and Validation
- **Run Tests**: After making the necessary adjustments, run your entire test suite to ensure that all tests pass without errors. Pay special attention to areas that use deprecated features or have been updated.
- **Monitor Logs**: Check logs for any warnings or errors