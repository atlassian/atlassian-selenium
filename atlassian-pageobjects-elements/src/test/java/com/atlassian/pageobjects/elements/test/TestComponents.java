package com.atlassian.pageobjects.elements.test;

import com.atlassian.pageobjects.elements.test.pageobjects.page.ElementsPage;
import com.atlassian.pageobjects.elements.test.pageobjects.page.JQueryPage;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.junit.Test;

import jakarta.inject.Inject;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestComponents extends AbstractPageElementBrowserTest
{
    @Inject
    private Timeouts timeouts;

    @Test
    public void testJqueryMenu()
    {
        JQueryPage jquerypage = product.visit(JQueryPage.class);

        // verify admin menu is present
        assertTrue(jquerypage.jqueryMenu().getTrigger().isPresent());

        // verify admin menu is not opened
        assertFalse(jquerypage.jqueryMenu().isOpen());

        // open the admin menu, verify is open.
        assertTrue(jquerypage.jqueryMenu().open().isOpen());

        // close the admin menu, verify not open
        assertFalse(jquerypage.jqueryMenu().close().isOpen());


        // verify follow link. Note: ElementsPage has a @WaitFor to verify that it reached the right page.
        ElementsPage auiPage = jquerypage.jqueryMenu().open().gotoElementsPage();

        // verify high level interactions
        jquerypage = product.visit(JQueryPage.class);
        jquerypage.openJqueryMenu().gotoElementsPage();

    }
}
