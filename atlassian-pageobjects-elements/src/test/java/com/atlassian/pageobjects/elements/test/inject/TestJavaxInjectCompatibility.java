package com.atlassian.pageobjects.elements.test.inject;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.test.pageobjects.PageElementsTestedProduct;
import com.atlassian.pageobjects.elements.test.pageobjects.page.ElementsPage;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import com.atlassian.webdriver.testing.rule.DefaultProductContextRules;
import com.atlassian.webdriver.testing.simpleserver.SimpleServerRunner;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@TestedProductClass(PageElementsTestedProduct.class)
@RunWith(SimpleServerRunner.class)
public class TestJavaxInjectCompatibility {
    @Inject
    protected static PageElementsTestedProduct product;
    @Inject
    protected static WebDriver driver;

    @Inject
    @ClassRule
    public static DefaultProductContextRules.ForClass classRules;
    @Inject
    @Rule
    public DefaultProductContextRules.ForMethod methodRules;
    @Inject
    private PageElementFinder elementFinder;

    @Test
    public void testComponentsAreInjected() {
        assertNotNull(PageElementsTestedProduct.class.getName() + " should be injected", product);
        assertNotNull(WebDriver.class.getName() + " should be injected", driver);
        assertNotNull(DefaultProductContextRules.ForClass.class.getName() + " should be injected", classRules);
        assertNotNull(DefaultProductContextRules.ForMethod.class.getName() + " should be injected", methodRules);
    }

    @Test
    public void testVisitProductPage() {
        ElementsPage elementsPage = product.visit(ElementsPage.class);

        elementsPage.test1_addElementsButton().click();

        Poller.waitUntilTrue(elementsPage.test1_delayedSpan().timed().isPresent());
    }

    @Test
    public void testIsPresent() {
        product.visit(ElementsPage.class);

        // Positive - verify element that exists
        assertTrue(product.find(By.id("test1_addElementsButton")).isPresent());

        // Negative - verify element that does not exist
        assertFalse(product.find(By.id("test1_non_existant")).isPresent());

        // Delayed presence - click on button that adds a span with delay, verify isPresent does not wait.
        product.find(By.id("test1_addElementsButton")).click();
        assertFalse(product.find(By.id("test1_delayedSpan")).isPresent());
    }

    @Test
    public void testIsVisible() {
        product.visit(ElementsPage.class);

        PageElement testInput = product.find(By.id("test2_input"));

        // Positive - verify input that is visible
        assertTrue(testInput.isVisible());

        // Delayed presence - click on a button that adds an element with delay, verify isVisible waits
        product.find(By.id("test2_addElementsButton")).click();
        assertTrue(product.find(By.id("test2_delayedSpan")).isVisible());

        // Delayed positive - click on button to make input visible with delay and verify that it did not wait
        product.find(By.id("test2_toggleInputVisibility")).click();
        product.find(By.id("test2_toggleInputVisibilityWithDelay")).click();
        assertFalse(testInput.isVisible());
    }

    @Test
    public void testGetText() {
        product.visit(ElementsPage.class);

        // Positive - verify span with text
        assertEquals("Span Value", product.find(By.id("test3_span")).getText());

        // Delayed presence - click on button that adds a span with delay, verify getText waits
        product.find(By.id("test3_addElementsButton")).click();
        assertEquals("Delayed Span", product.find(By.id("test3_delayedSpan")).getText());

        // Delayed positive - click on button that sets the text of span with delay, verify getText does not wait
        product.find(By.id("test3_setTextButton")).click();
        assertEquals("", product.find(By.id("test3_spanEmpty")).getText());
    }

    @Test
    public void testElementFinder() {
        product.visit(ElementsPage.class);
        PageElement button = elementFinder.find(By.id(("test11_button")));

        assertFalse(button.hasClass("test"));
    }

}
